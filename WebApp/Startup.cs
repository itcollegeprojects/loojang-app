﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BL.Factories;
using BL.Factories.Interfaces;
using BL.Services;
using BL.Services.Interfaces;
using DAL.App.EF;
using DAL.App.EF.Helpers;
using DAL.App.Interfaces;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Services;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            // AddPatientToRoom application services.
            services.AddTransient<IEmailSender, EmailSender>();

            // AddPatientToRoom uow to DI container
            // scoped - object lives for duration of web request
            // transient - created new on every new object creation
            // singleton - created once, lives forever

            services.AddSingleton<IRepositoryFactory, EFRepositoryFactory>();
            services.AddScoped<IRepositoryProvider, EFRepositoryProvider>();
            services.AddScoped<IDataContext, DAL.App.EF.ApplicationDbContext>();
            services.AddScoped<IAppUnitOfWork, AppEFUnitOfWork>();
            services.AddScoped<IdentityDbContext<Domain.ApplicationUser>, DAL.App.EF.ApplicationDbContext>();

            // Services injection mapping
            services.AddScoped<IApplicationUserService, ApplicationUserService>();
            services.AddScoped<IFrequencyTypeService, FrequencyTypeService>();
            services.AddScoped<IMedicalCaseService, MedicalCaseService>();
            services.AddScoped<IPatientRoomService, PatientRoomService>();
            services.AddScoped<IPatientsDoctorService, PatientsDoctorService>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<IPatientsGuardianService, PatientsGuardianService>();
            services.AddScoped<IPrescriptionService, PrescriptionService>();
            services.AddScoped<IProcedureService, ProcedureService>();
            services.AddScoped<IRoomService, RoomService>();   
            services.AddScoped<IAppendStartupDataService, AppendStartupDataService>();

            // Factories injection mapping
            services.AddScoped<IApplicationUserFactory, ApplicationUserFactory>();
            services.AddScoped<IFrequencyTypeFactory, FrequencyTypeFactory>();
            services.AddScoped<IMedicalCaseFactory, MedicalCaseFactory>();
            services.AddScoped<IPatientFactory, PatientFactory>();
            services.AddScoped<IPatientBriefFactory, PatientBriefFactory>();
            services.AddScoped<IPatientToRoomFactory, PatientToRoomFactory>();
            services.AddScoped<IPatientsRoomFactory, PatientsRoomFactory>();
            services.AddScoped<IRoomsPatientFactory, RoomsPatientFactory>();
            services.AddScoped<IPatientsDoctorFactory, PatientsDoctorFactory>();
            services.AddScoped<IPatientToDoctorFactory, PatientToDoctorFactory>();
            services.AddScoped<IPatientsDoctorDetailsFactory, PatientsDoctorDetailsFactory>();
            services.AddScoped<IPatientsGuardianDetailsFactory, PatientsGuardianDetailsFactory>();
            services.AddScoped<IPatientsGuardianFactory, PatientsGuardianFactory>();
            services.AddScoped<IPrescriptionFactory, PrescriptionFactory>();
            services.AddScoped<IPrescriptionDescriptionFactory, PrescriptionDescriptionFactory>();
            services.AddScoped<IProcedureFactory, ProcedureFactory>();
            services.AddScoped<IRoomFactory, RoomFactory>();
            services.AddScoped<IRoomCodeFactory, RoomCodeFactory>();

            #region add xml support
            //Respect browser headers
            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true; // false by default
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddMvc().AddXmlSerializerFormatters();
            #endregion

            #region jsonconfiguration
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling
                            = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
                options.SerializerSettings.PreserveReferencesHandling
                            = Newtonsoft.Json.PreserveReferencesHandling.None;
                options.SerializerSettings.Formatting
                            = Newtonsoft.Json.Formatting.Indented;
            });

            #endregion


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext<Domain.ApplicationUser>>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // VERY WEAK Password settings for testing
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6; // if you change this, then also update viewmodel atrributes
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 0;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;

            });


            services.AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })

                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["Token:Issuer"],
                        ValidAudience = Configuration["Token:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["Token:Key"])
                            )
                    };

                    // if you wish to modify identity (ie validate that use is not banned)
                    options.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async (context) =>
                        {

                            // lets check from usermanager, is the user actually allowed into system
                            var userManager = context.HttpContext.RequestServices.GetService<UserManager<ApplicationUser>>();
                            var user = await userManager.FindByEmailAsync(context.Principal.Identity.Name);                            
                            if (user == null || user.LockoutEnd > DateTime.Now)
                            {
                                context.Response.StatusCode = 401;
                            }

                        }                        
                };
                });


            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminOnly", policy => policy.RequireClaim(ClaimTypes.Role, "Admin"));
                options.AddPolicy("DoctorOnly", policy => policy.RequireClaim(ClaimTypes.Role, "Admin", "Doctor"));
                options.AddPolicy("EmployeeOnly", policy => policy.RequireClaim(ClaimTypes.Role, "Admin", "Caretaker", "Doctor"));
                options.AddPolicy("UserWithRole", policy => policy.RequireClaim(ClaimTypes.Role, "Admin", "Caretaker", "Doctor", "Guardian"));
                options.AddPolicy("PatientGuardian", policy => policy.RequireClaim(ClaimTypes.Role, "Admin", "Guardian"));
            });

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "KuldneLoojang API", Version = "v1" });
            });


            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });


            // Return 401 when unauthorized via API instead of 302 redirect to login page
            services.ConfigureApplicationCookie(options =>
            {
                options.Events.OnRedirectToLogin = context =>
                {
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider services, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseAuthentication();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "KuldneLoojang V1");
            });

            app.UseCors("CorsPolicy");


            app.UseMvc();

            // Create roles and superuser, if do not exist
            CreateUserRoles(services).Wait();

            // https://nblumhardt.com/2016/10/aspnet-core-file-logger/
            loggerFactory.AddFile("Logs/{Date}.log");

        }

        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {

            var users = new List<IdentityUserWithRoles>()
            {
                new IdentityUserWithRoles(name: "all@eesti.ee", email: "all@eesti.ee", roles: new[] { "Admin", "Caretaker", "Doctor", "Guardian" }, password: "aaaaaa"),
                new IdentityUserWithRoles(name: "admin@eesti.ee", email: "admin@eesti.ee", roles: new[] { "Admin" }, password: "aaaaaa"),
                new IdentityUserWithRoles(name: "caretaker@eesti.ee", email: "caretaker@eesti.ee", roles: new[] { "Caretaker" }, password: "aaaaaa"),
                new IdentityUserWithRoles(name: "doctor@eesti.ee", email: "doctor@eesti.ee", roles: new[] { "Doctor" }, password: "aaaaaa"),
                new IdentityUserWithRoles(name: "guardian@eesti.ee", email: "guardian@eesti.ee", roles: new[] { "Guardian" }, password: "aaaaaa"),
                new IdentityUserWithRoles(name: "roleless@eesti.ee", email: "roleless@eesti.ee", roles:null, password: "aaaaaa"),
            };

            RoleManager<IdentityRole> roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            UserManager<ApplicationUser> userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            // managers can be null?, maybe there is no identity support
            if (roleManager == null || userManager == null)
            {
                return;
            }

            foreach (var appuser in users)
            {
                foreach (var roleName in appuser.Roles)
                {
                    var roleExists = await roleManager.RoleExistsAsync(roleName);
                    if (!roleExists)
                    {
                        await roleManager.CreateAsync(new IdentityRole(roleName));
                    }
                }

                var user = await userManager.FindByEmailAsync(email: appuser.User.NormalizedEmail);
                if (user == null)
                {
                    var res = await userManager.CreateAsync(user: appuser.User);
                    user = await userManager.FindByEmailAsync(email: appuser.User.NormalizedEmail);
                }

                var resAddToRoles = await userManager.AddToRolesAsync(user: user, roles: appuser.Roles);
            }
        }

        // https://stackoverflow.com/questions/42471866/how-to-create-roles-in-asp-net-core-and-assign-them-to-users?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        public async Task CreateRoles(IServiceProvider services)
        {
            //initializing custom roles 
            var RoleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = services.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "Admin", "Doctor", "Patient", "Caretaker", "Guardian" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            //Here you could create a super user who will maintain the web app
            var poweruser = new ApplicationUser
            {

                UserName = Configuration["AdminUser:UserName"],
                Email = Configuration["AdminUser:UserEmail"],
            };
            //Ensure you have these values in your appsettings.json file
            string userPWD = Configuration["AdminUser:Password"];
            var _user = await UserManager.FindByEmailAsync(Configuration["AdminUser:AdminUserEmail"]);

            if (_user == null)
            {
                var createPowerUser = await UserManager.CreateAsync(poweruser, userPWD);
                if (createPowerUser.Succeeded)
                {
                    //here we tie the new user to the role
                    await UserManager.AddToRoleAsync(poweruser, "Admin");
                }
            }
        }
    }

    public class IdentityUserWithRoles
    {
        private readonly ApplicationUser _user;
        public ApplicationUser User => _user;
        public string[] Roles { get; set; }
        public IdentityUserWithRoles(string name, string email, string[] roles = null, string password = "parool")
        {
            Roles = roles ?? new string[] { };
            _user = new ApplicationUser
            {
                Email = email,
                NormalizedEmail = email.ToUpper(),
                NormalizedUserName = email.ToUpper(),
                UserName = name,
                SecurityStamp = Guid.NewGuid().ToString(format: "D"),
                FirstName = "John",
                LastName = "Doe",

            };
            _user.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(user: _user, password: password);
        }
    }
}
