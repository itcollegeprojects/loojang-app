﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/frequencytypes")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FrequencyTypesController : Controller
    {
        private readonly IFrequencyTypeService _frequencyTypeService;

        public FrequencyTypesController(IFrequencyTypeService frequencyTypeService)
        {
            _frequencyTypeService = frequencyTypeService;
        }

        // GET: api/frequencyTypes
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<FrequencyTypeDTO>), 200)]
        [Authorize(Policy = "EmployeeOnly")]
        public IEnumerable<FrequencyTypeDTO> GetFrequencyTypes()
        {
            return _frequencyTypeService.All();
        }

        // GET: api/frequencyTypes/5
        [HttpGet("{frequencyTypeId:int}")]
        [ProducesResponseType(typeof(FrequencyTypeDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult GetFrequencyTypeById(int frequencyTypeId)
        {
            var r = _frequencyTypeService.Find(frequencyTypeId);
            if (r == null) return NotFound();
            return Ok(r);
        }

        // POST: api/frequencyTypes
        [HttpPost]
        [ProducesResponseType(typeof(FrequencyTypeDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddFrequencyType([FromBody]FrequencyTypeDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var ret = _frequencyTypeService.Add(p);

            return Ok(ret);
        }

        // PATCH: api/frequencyTypes/5
        [HttpPatch("{frequencyTypeId:int}")]    
        [ProducesResponseType(typeof(FrequencyTypeDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult PatchFrequencyType(int frequencyTypeId, [FromBody]FrequencyTypeDTO p)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            p.FrequencyTypeId = frequencyTypeId;

            var ret = _frequencyTypeService.Update(p);
            if (ret != null)
            {
                return Ok(ret);
            }

            return NotFound("Frequency type not found");
        }

        [HttpDelete("{frequencyTypeId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult DeleteFrequencyType(int frequencyTypeId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _frequencyTypeService.Delete(frequencyTypeId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound("Frequency Type not found");
        }

    }
}
