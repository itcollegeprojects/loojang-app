﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/patientrooms")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PatientRoomsController : Controller
    {

        private readonly IPatientRoomService _patientRoomService;
        private readonly IPatientRoomService _roomService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUserService _applicationUserService;

        public PatientRoomsController(IPatientRoomService patientRoomService, IPatientRoomService roomService, Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> userManager, IApplicationUserService applicationUserService)
        {
            _patientRoomService = patientRoomService;
            _roomService = roomService;
            _userManager = userManager;
            _applicationUserService = applicationUserService;
        }


        // POST: api/patientsroom
        [HttpPost]
        [ProducesResponseType(typeof(PatientToRoomDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult AddPatientToRoom([FromBody]PatientToRoomDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();
            try
            {
                var ret = _patientRoomService.AddPatientToRoom(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }


        // GET: api/patientrooms/5
        [HttpGet("{patientRoomId:int}")]
        [ProducesResponseType(typeof(PatientToRoomDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> Get(int patientRoomId)
        {
            PatientToRoomDTO r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _patientRoomService.Find(patientRoomId);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                    r = _patientRoomService.Find(patientRoomId, user.ApplicationUserId);

                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
            if (r == null) return NotFound("Patient room not found");
            return Ok(r);
        }

        // DELETE: api/patientrooms/5
        [HttpDelete("{patientRoomId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Delete(int patientRoomId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _patientRoomService.Delete(patientRoomId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound("Patientroom not found");
        }

        // PATCH: api/patientroom/5
        [HttpPatch("{patientRoomId:int}")]
        [ProducesResponseType(typeof(PatientToRoomDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Patch(int patientRoomId, [FromBody]PatientToRoomDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            p.PatientRoomId = patientRoomId;

            try
            {
                var ret = _patientRoomService.Update(p);
                if (ret != null)
                {
                    return Ok(ret);
                }
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }

            return NotFound("Patients room not found");

        }

        // GET: api/patientrooms
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PatientToRoomDTO>), 200)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> GetAsync()
        {
            IEnumerable<PatientToRoomDTO> r;
            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _patientRoomService.All();
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);
                r = _patientRoomService.All(user.ApplicationUserId);
            }
            else
            {
                return Unauthorized();
            }

            return Ok(r);
        }

    }
}