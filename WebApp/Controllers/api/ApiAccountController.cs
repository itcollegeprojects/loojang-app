﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Models.AccountViewModels;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/account")]
    [AllowAnonymous]
    public class ApiAccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ApiAccountController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }


        [HttpPost]
        [Route("register")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Register([FromBody] RegisterDTO p)
        {

            if (p.Password != p.ConfirmPassword)
            {
                return BadRequest("Password and confirmation do not match");
            }

            if (_userManager.FindByEmailAsync(p.Email).Result != null) return BadRequest("User already exists");

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = p.Email, Email = p.Email, FirstName = p.Firstname, LastName = p.LastName };
                var result = await _userManager.CreateAsync(user, p.Password);
                if (result.Succeeded)
                {
                    return Ok();
                }
            }
                        
            return BadRequest("Could not create user");
        }

    }
}