﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Internal.System.Collections.Sequences;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/users")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ApplicationUsersController : Controller
    {
        private readonly IApplicationUserService _applicationUserService;
        private readonly IPatientsDoctorService _patientsDoctorService;
        private readonly IPatientsGuardianService _patientsGuardianService;
        private readonly IMedicalCaseService _medicalCaseService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> _userManager;

        public ApplicationUsersController(
            IApplicationUserService applicationUserService,
            IPatientsDoctorService patientsDoctorService,
            IMedicalCaseService medicalCaseService,
            Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> userManager,
            IPatientsGuardianService patientsGuardianService)
        {
            _applicationUserService = applicationUserService;
            _patientsDoctorService = patientsDoctorService;
            _medicalCaseService = medicalCaseService;
            _userManager = userManager;
            _patientsGuardianService = patientsGuardianService;
        }

        // GET: api/users/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(404)]
        // Every user can access their own user data
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult Get(string id)
        {
            var r = _applicationUserService.Find(id);
            if (r != null) return Ok(r);
            return NotFound();
        }

        [HttpGet]
        [Route("self")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(404)]
        // Every user can access their own user data
        [Authorize]
        public async Task<IActionResult> GetOwnUser()
        {
            var id = User.Identity.GetUserId();
            var user = await _userManager.FindByEmailAsync(id);
            var r = _applicationUserService.Find(user.Id);
            if (r != null) return Ok(r);

            return NotFound();
        }

        // DELETE: api/users/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy ="AdminOnly")]
        public IActionResult Delete(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var applicationUserId = _userManager.FindByNameAsync("admin@eesti.ee").Result.Id;
            if (applicationUserId == id) return Ok("Not allowed!");

            var ret = _applicationUserService.Delete(id);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // POST: api/users/5/roles
        [HttpPost("{id}/roles/{role}")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult PostRoles(string id, string role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var applicationUserId = _userManager.FindByNameAsync("admin@eesti.ee").Result.Id;
            if (applicationUserId == id) return Ok("Not allowed!");

            var r = _applicationUserService.Find(id);
            if (r == null)
            {
                return NotFound("User or role not found");
            }

            var ret = _applicationUserService.AddToRole(r.ApplicationUserId, role);

            if (ret != null)
            {
                return Ok(ret);
            }

            return NotFound("User or role not found");
        }

        // GET: api/users/{id}/roles/remove/{role}
        [HttpGet("{id}/roles/remove/{role}")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult RemoveFromRole(string id, string role)
        {           
            var ret = _applicationUserService.RemoveFromRole(id, role);
            if (ret != null) return Ok(ret);            

            return NotFound("User or role not found");
        }

        // GET: api/users/{id}/roles/add/{role}
        [HttpGet("{id}/roles/add/{role}")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult AddToRole(string id, string role)
        {
            
            var ret = _applicationUserService.AddToRole(id, role);

            if (ret != null) return Ok(ret);

            return NotFound("User or role not found");
        }
        
        // PATCH: api/users/5
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(ApplicationUserDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Patch(string id, [FromBody]ApplicationUserDTO p)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            p.ApplicationUserId = id;

            var ret = _applicationUserService.Update(p);
            if (ret != null)
            {
                return Ok(ret);
            }

            return NotFound("User not found");
        }

        // GET: api/users/{userRole}
        [HttpGet("roles/{userRole}")]
        [ProducesResponseType(typeof(IEnumerable<ApplicationUserDTO>), 200)]
        [ProducesResponseType(401)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult GetByRole(string userRole)
        {
            var r = _applicationUserService.AllByRole(userRole.ToUpper());
            if (r == null) return NotFound();
            return Ok(r);
        }

        // GET: api/users/
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ApplicationUserDTO>), 200)]
        [ProducesResponseType(401)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Get()
        {
            ClaimsPrincipal currentUser = this.User;
            var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;

            var adminUserId = _userManager.FindByNameAsync("admin@eesti.ee").Result.Id;

            var r = _applicationUserService
                .All()
                .Where(u => u.ApplicationUserId != applicationUserId && u.ApplicationUserId != adminUserId)
                .ToList();

            if (r == null) return NotFound();
            if (!r.Any()) return NoContent();
            return Ok(r);
        }

        // GET: api/users/5/medicalcases
        [HttpGet("{id}/cases")]
        [ProducesResponseType(typeof(IEnumerable<MedicalCaseDTO>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult GetPatientMedicalcases(string id)
        {
            IEnumerable<PatientsDoctorDetailsDTO> patientsDoctor = _patientsDoctorService.FindByApplicationUserId(id);
            if (patientsDoctor == null) return NotFound($"Doctor with id {id} was not found");
            if (!patientsDoctor.Any()) return Ok(new ArrayList<MedicalCaseDTO>());



            //generate list of medicalcases
            List<MedicalCaseDTO> medicalCase = null;
            foreach (PatientsDoctorDetailsDTO pd in patientsDoctor)
            {
                medicalCase.AddRange(_medicalCaseService.FindByPatientsDoctorsId(pd.PatientsDoctorId).ToList());
            }

            return Ok(medicalCase);
        }

    }

    public class RoleDTO
    {
        public string Role { get; set; }
    }
}
