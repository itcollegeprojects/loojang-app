﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/PrescriptionFrequencyProcedure")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PrescriptionFrequencyProcedureController : Controller
    {
        private readonly IPrescriptionService _prescriptionService;

        public PrescriptionFrequencyProcedureController(IPrescriptionService prescriptionService)
        {
            _prescriptionService = prescriptionService;
        }

        // POST: api/PrescriptionFrequencyProcedure
        [HttpPost]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddWithProcedures([FromBody]PrescriptionDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var ret = _prescriptionService.AddWithProcedures(p);
            return Ok(ret);
        }
    }
}