﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/patientsdoctors")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PatientsDoctorsController : Controller
    {
        private readonly IPatientsDoctorService _patientsDoctorService;
        private readonly IMedicalCaseService _medicalCaseService;
        private readonly UserManager<ApplicationUser> _userManager;

        public PatientsDoctorsController(
            IPatientsDoctorService patientsDoctorService, 
            IMedicalCaseService medicalCaseService,
            UserManager<ApplicationUser> userManager
            )
        {
            _patientsDoctorService = patientsDoctorService;
            _medicalCaseService = medicalCaseService;
            _userManager = userManager;
        }

        // GET: api/patientsDoctors
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PatientsDoctorDTO>), 200)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetPatientsDoctors()
        {            

            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                var r = _patientsDoctorService.FindByApplicationUserId(applicationUserId);
                return Ok(r);
            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                var r = _patientsDoctorService.All();
                return Ok(r);
            }
            else
            {
                return Unauthorized();
            }            
        }

        // GET: api/patientsdoctors/5
        [HttpGet("{patientsDoctorId:int}")]
        [ProducesResponseType(typeof(PatientsDoctorDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetPatientsDoctorById(int patientsDoctorId)
        {
            PatientsDoctorDTO r;

            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                r = _patientsDoctorService.FindByApplicationUserIdAndId(applicationUserId, patientsDoctorId);

            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                r = _patientsDoctorService.Find(patientsDoctorId);
            }
            else
            {
                return Unauthorized();
            }

            if (r == null) return NotFound();

            return Ok(r);
        }

        // POST: api/patientsdoctors
        [HttpPost]
        [ProducesResponseType(typeof(PatientToDoctorDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddPatientToDoctor([FromBody]PatientToDoctorDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                var ret = _patientsDoctorService.Add(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // PATCH: api/patientsDoctors/5
        [HttpPatch("{patientsDoctorId:int}")]
        [ProducesResponseType(typeof(PatientsDoctorDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult PatchPatientsDoctor(int patientsDoctorId, [FromBody]PatientsDoctorDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            p.PatientsDoctorId = patientsDoctorId;

            try
            {
                var ret = _patientsDoctorService.Update(p);
                if (ret != null)
                {
                    return Ok(ret);
                }
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
            return NotFound("Patients Doctor not found");
        }

        // DELETE: api/patientsdoctors/5
        [HttpDelete("{patientsDoctorId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult DeletePatientsDoctor(int patientsDoctorId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _patientsDoctorService.Delete(patientsDoctorId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // POST: api/patientsDoctors/5/medicalCases
        [HttpPost("{patientsDoctorsId:int}/medicalCases")]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddMedicalCase(int patientsDoctorsId, [FromBody]MedicalCaseDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var r = _patientsDoctorService.Find(patientsDoctorsId);
            if (r == null) return NotFound($"PatientDoctor with id {patientsDoctorsId} not found");

            p.PatientsDoctorId = r.PatientsDoctorId;

            var ret = _medicalCaseService.Add(p);

            return Ok(ret);
        }
    }
}
