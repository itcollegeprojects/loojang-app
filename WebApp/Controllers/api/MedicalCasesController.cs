﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Loggers;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/medicalcases")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MedicalCasesController : Controller
    {
        private readonly IMedicalCaseService _medicalCaseService;
        private readonly IPrescriptionService _prescriptionService;
        private readonly IApplicationUserService _applicationUserService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;


        public MedicalCasesController(
            IMedicalCaseService medicalCaseService, 
            IPrescriptionService prescriptionService, 
            Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> userManager, 
            IApplicationUserService applicationUserService,
            ILogger<MedicalCasesController> logger)
        {
            _medicalCaseService = medicalCaseService;
            _prescriptionService = prescriptionService;
            _userManager = userManager;
            _applicationUserService = applicationUserService;
            _logger = logger;
        }

        // GET: api/medicalcases
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<MedicalCaseDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> GetMedicalCases()
        {
            IEnumerable<MedicalCaseDTO> r;

            if (User.IsInRole("Admin"))
            {
                r = _medicalCaseService.All();
            }
            else if (User.IsInRole("Doctor"))
            {
                var id = User.Identity.GetUserId();
                r = _medicalCaseService.FindByDoctorId(id);
                return Ok(r);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);
                r = _medicalCaseService.AllByGuardianId(user.ApplicationUserId);
            }
            else
            {
                return Unauthorized();
            }

            return Ok(r);
        }

        // GET: api/medicalcases/5
        [HttpGet("{medicalCaseId:int}")]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> GetMedicalCaseById(int medicalCaseId)
        {
            MedicalCaseDTO r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _medicalCaseService.Find(medicalCaseId);
                // _logger.LogInformation(LoggingEvents.GetItem, "{string} - {date}", r.ToString(), DateTime.Now);

            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                    r = _medicalCaseService.FindForGuardian(medicalCaseId, user.ApplicationUserId);

                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
            if (r == null) return NotFound("Medical case not found");
            return Ok(r);
        }


        // GET: api/medicalcases/5/prescriptions
        [HttpGet("{medicalCaseId:int}/prescriptions")]
        [ProducesResponseType(typeof(IEnumerable<PrescriptionDescriptionDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> GetPrescriptionsByMedicalCaseId(int medicalCaseId)
        {
            IEnumerable<PrescriptionDescriptionDTO> r = new LinkedList<PrescriptionDescriptionDTO>();

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _prescriptionService.FindPrescriptionDescriptionsByMedicalCaseId(medicalCaseId);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                    var m = _medicalCaseService.FindForGuardian(medicalCaseId, user.ApplicationUserId);
                    if (m != null)
                    {
                        r = _prescriptionService.FindPrescriptionDescriptionsByMedicalCaseId(medicalCaseId);
                    }
                    
                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

            if (r == null) return NotFound("Prescriptions not found");
            return Ok(r);
        }


        // GET: api/medicalcases/doctor/5
        [HttpGet("doctor/{doctorId}")]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult GetMedicalCaseByDoctorId(string doctorId)
        {
            var r = _medicalCaseService.FindByDoctorId(doctorId);
            if (r == null) return NotFound();
            return Ok(r);
        }

        // GET: api/medicalcases/patient/5
        [HttpGet("patient/{patientId:int}")]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetMedicalCaseByPatientId(int patientId)
        {
            var r = _medicalCaseService.FindByPatientId(patientId);
            if (r == null) return NotFound();
            return Ok(r);
        }


        // POST: api/medicalcases
        [HttpPost]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddMedicalCase([FromBody]MedicalCaseDTO p)
        {
            if (!ModelState.IsValid) return BadRequest("Bad model!");

            try
            {
                var ret = _medicalCaseService.Add(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // PATCH: api/medicalcases/5
        [HttpPatch("{medicalCaseId:int}")]
        [ProducesResponseType(typeof(MedicalCaseDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult PatchMedicalCase(int medicalCaseId, [FromBody]MedicalCaseDTO p)
        {
            if (!ModelState.IsValid) return BadRequest("Bad model");//ModelState);

            p.MedicalCaseId = medicalCaseId;

            try
            {
                var ret = _medicalCaseService.Update(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // DELETE: api/medicalCases/5
        [HttpDelete("{medicalCaseId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult DeleteMedicalCase(int medicalCaseId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _medicalCaseService.Delete(medicalCaseId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // POST: api/medicalcases/5/prescriptions
        [HttpPost("{medicalCaseId:int}/prescriptions")]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddPrescription(int medicalCaseId, [FromBody]PrescriptionDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var r = _medicalCaseService.Find(medicalCaseId);
            if (r == null) return NotFound($"Medicalcase with id {medicalCaseId} not found");

            p.MedicalCaseId = r.MedicalCaseId;

            var ret = _prescriptionService.Add(p);

            return Ok(ret);
        }
    }
}
