﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using WebApp.Loggers;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/patients")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PatientsController : Controller
    {
        private readonly IPatientService _patientService;
        private readonly IPatientsDoctorService _patientsDoctorService;
        private readonly IPatientsGuardianService _patientsGuardianService;
        private readonly IMedicalCaseService _medicalCaseService;
        private readonly IApplicationUserService _applicationUserService;
        private readonly IServiceProvider _serviceProvider;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private readonly IPatientsDoctorDetailsFactory _patientsDoctorDetailsFactory;
        private readonly IPatientsDoctorFactory _patientsDoctorFactory;
        private readonly IPatientsGuardianDetailsFactory _patientsGuardianDetailsFactory;
        private readonly IPatientsGuardianFactory _patientsGuardianFactory;
        private readonly IPatientRoomService _patientRoomService;

        public PatientsController(
            IPatientService patientService,
            IPatientsDoctorService patientsDoctorService,
            IMedicalCaseService medicalCaseService,
            IApplicationUserService applicationUserService,
            IServiceProvider serviceProvider,
            UserManager<ApplicationUser> userManager,
            ILogger<PatientsController> logger,
            IPatientsDoctorDetailsFactory patientsDoctorDetailsFactory,
            IPatientsDoctorFactory patientsDoctorFactory,
            IPatientRoomService patientRoomService,
            IPatientsGuardianService patientsGuardianService, IPatientsGuardianFactory patientsGuardianFactory, IPatientsGuardianDetailsFactory patientsGuardianDetailsFactory)
        {
            _patientService = patientService;
            _patientsDoctorService = patientsDoctorService;
            _medicalCaseService = medicalCaseService;
            _applicationUserService = applicationUserService;
            _serviceProvider = serviceProvider;
            _userManager = userManager;
            _logger = logger;
            _patientsDoctorDetailsFactory = patientsDoctorDetailsFactory;
            _patientsDoctorFactory = patientsDoctorFactory;
            _patientRoomService = patientRoomService;
            _patientsGuardianService = patientsGuardianService;
            _patientsGuardianFactory = patientsGuardianFactory;
            _patientsGuardianDetailsFactory = patientsGuardianDetailsFactory;
        }

        // GET: api/patients
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PatientDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult Get()
        {
            ClaimsPrincipal currentUser = this.User;
            var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
            currentUser.HasClaim(ClaimTypes.Role, "Patient");

            
            IEnumerable<PatientDTO> r = Enumerable.Empty<PatientDTO>();
            
            var identity = (ClaimsIdentity)User.Identity;

            _logger.LogInformation(LoggingEvents.GetItem, "Getting all items at {date}", DateTime.Now);

            if (identity.HasClaim(ClaimTypes.Role, "Admin"))
            {
                r = _patientService.All();
            } else if (identity.HasClaim(ClaimTypes.Role, "Guardian") || identity.HasClaim(ClaimTypes.Role, "Patient"))
            {
                r = _patientService.AllByGuardianId(applicationUserId);
            } else if (identity.HasClaim(ClaimTypes.Role, "Doctor"))
            {
                r = _patientService.AllByDoctorsId(applicationUserId);
            } else if (identity.HasClaim(ClaimTypes.Role, "Caretaker"))
            {
                r = _patientService.AllPresentPatients();
            }

            return Ok(r);
        }


        // GET: api/patients
        [HttpGet("role/{role}")]
        [ProducesResponseType(typeof(IEnumerable<PatientDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetByRole(string role)
        {

            role = role.ToLower();

            IEnumerable<PatientDTO> r = Enumerable.Empty<PatientDTO>();

            ClaimsPrincipal currentUser = this.User;
            var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
            currentUser.HasClaim(ClaimTypes.Role, "Patient");
            
            var identity = (ClaimsIdentity)User.Identity;

            _logger.LogInformation(LoggingEvents.GetItem, "Getting all items by role ({role}) at {date}", role, DateTime.Now);


            switch (role)
            {
                case "admin":
                    if (identity.HasClaim(ClaimTypes.Role, "Admin"))
                    {
                        r = _patientService.All();
                    }
                    break;
                case "guardian":
                    if (identity.HasClaim(ClaimTypes.Role, "Guardian") || identity.HasClaim(ClaimTypes.Role, "Patient"))
                    {
                        r = _patientService.AllByGuardianId(applicationUserId);
                    }

                    break;
                case "caretaker":
                    if (identity.HasClaim(ClaimTypes.Role, "Caretaker"))
                    {
                        r = _patientService.AllPresentPatients();
                    }
                    break;
                case "doctor":
                    if (identity.HasClaim(ClaimTypes.Role, "Doctor"))
                    {
                        r = _patientService.AllByDoctorsId(applicationUserId);
                    }
                    break;

            }
        
            return Ok(r.Distinct());
        }

        // GET: api/patients/5
        [HttpGet("{patientId:int}")]
        [ProducesResponseType(typeof(PatientDTO), 200)]        
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult Get(int patientId)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting item {ID} at {date}", patientId, DateTime.Now);
            PatientDTO r;

            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                r = _patientService
                    .AllByGuardianId(applicationUserId)
                    .FirstOrDefault(p => p.PatientId == patientId);
            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                r = _patientService.Find(patientId);
            } else
            {
                return Unauthorized();
            }


            if (r == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetById({ID}) NOT FOUND", patientId);
                return NotFound("Patient not found");
            }
            _logger.LogInformation(LoggingEvents.GetItem, "Got item {ID}", patientId);
            return Ok(r);
        }

        // POST: api/patients
        [HttpPost]
        [ProducesResponseType(typeof(PatientDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddPatient([FromBody]PatientDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();
            var ret = _patientService.Add(p);
            return Ok(ret);
        }

        // GET: api/patients/5/rooms
        [HttpGet("{patientId:int}/rooms")]
        [ProducesResponseType(typeof(IEnumerable<RoomsPatientDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetRooms(int patientId)
        {

            PatientDTO r;

            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                r = _patientService
                    .AllByGuardianId(applicationUserId)
                    .FirstOrDefault(p => p.PatientId == patientId);
            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                r = _patientService.Find(patientId);
            }
            else
            {
                return Unauthorized();
            }

            if (r == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetById({ID}) NOT FOUND", patientId);
                return NotFound("Patient not found");
            }
        
            var rooms = _patientRoomService.FindByPatientId(patientId);
            return Ok(rooms);

        }

        // GET: api/patients/5/doctors
        [HttpGet("{patientId:int}/doctors")]
        [ProducesResponseType(typeof(IEnumerable<PatientsDoctorDetailsDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetDoctors(int patientId)
        {
            ClaimsPrincipal currentUser = this.User;
            var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            ApplicationUser user = _userManager.FindByNameAsync(currentUserName).Result;
            var applicationUserId = user.Id;

            
            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                var patientsDoctor = _patientsDoctorService.FindByApplicationUserIdAndPatientId(applicationUserId, patientId);
                return Ok(patientsDoctor);
            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                var patientsDoctor = _patientsDoctorService.FindByPatientId(patientId);
                return Ok(patientsDoctor);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/patients/5/guardians
        [HttpGet("{patientId:int}/guardians")]
        [ProducesResponseType(typeof(IEnumerable<PatientsGuardianDetailsDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetGuardians(int patientId)
        {
            
            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                var patientsGuardian = _patientsGuardianService.FindByApplicationUserIdAndPatientId(applicationUserId, patientId);
                return Ok(patientsGuardian);
            }
            else if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                var patientsGuardian = _patientsGuardianService.FindByPatientId(patientId);
                return Ok(patientsGuardian);
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/patients/5
        [HttpDelete("{patientId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Delete(int patientId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _patientService.Delete(patientId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound("Patient not found");
        }

        // PATCH: api/patients/5
        [HttpPatch("{patientId:int}")]
        [ProducesResponseType(typeof(PatientDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult Patch(int patientId, [FromBody]PatientDTO p)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            p.PatientId = patientId;

            var ret = _patientService.Update(p);
            if (ret != null)
            {
                return Ok(ret);
            }

            return NotFound("Patient not found");
        }

        // GET: api/patients/5/medicalCases
        [HttpGet("{patientId:int}/medicalCases")]
        [ProducesResponseType(typeof(IEnumerable<MedicalCaseDTO>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult GetPatientMedicalcases(int patientId)
        {
            IEnumerable<PatientsDoctorDetailsDTO> patientsDoctor;
            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
                ClaimsPrincipal currentUser = this.User;
                var currentUserName = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                var applicationUserId = _userManager.FindByNameAsync(currentUserName).Result.Id;
                patientsDoctor = _patientsDoctorService.FindByApplicationUserIdAndPatientId(applicationUserId,patientId);
            }
            else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
            {
                patientsDoctor = _patientsDoctorService.FindByPatientId(patientId);
            }
            else
            {
                return Unauthorized();
            }

            if (patientsDoctor == null) return NotFound($"Patient with id {patientId} was not found");

            List<MedicalCaseDTO> medicalCase=null;

            foreach (PatientsDoctorDetailsDTO pd in patientsDoctor)
            {
                medicalCase.AddRange(_medicalCaseService.FindByPatientsDoctorsId(pd.PatientsDoctorId).ToList());
            }

            return Ok(medicalCase);
        }
    }
}
