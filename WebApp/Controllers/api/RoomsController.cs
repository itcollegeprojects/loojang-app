﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/rooms")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoomsController : Controller
    {
        private readonly IRoomService _roomService;
        private readonly IPatientRoomService _patientRoomService;


        public RoomsController(IRoomService roomService, IPatientRoomService patientRoomService)
        {
            _roomService = roomService;
            _patientRoomService = patientRoomService;
        }

        // GET: api/rooms
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RoomDTO>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult GetRooms()
        {
            var r = _roomService.All();
            if (r == null) return NotFound();
            if (!r.Any()) return NoContent();
            return Ok(r);
        }

        // GET: api/rooms/free
        [HttpGet("free")]
        [ProducesResponseType(typeof(IEnumerable<RoomDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult GetFreeRooms()
        {
            var r = _roomService.GetFreeRooms();
            if (!r.Any()) return NoContent();
            return Ok(r);
        }

        // GET: api/rooms/5
        [HttpGet("{roomId:int}")]
        [ProducesResponseType(typeof(RoomDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult GetRoomById(int roomId)
        {
            var r = _roomService.Find(roomId);
            if (r == null) return NotFound("Room not found");
            return Ok(r);
        }

        // POST: api/rooms
        [HttpPost]
        [ProducesResponseType(typeof(RoomDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult AddRoom([FromBody]RoomDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var ret = _roomService.Add(p);

            return Ok(ret);
        }

        [HttpPost("{roomId:int}/occupancies")]
        [ProducesResponseType(typeof(PatientsRoomDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult GetOccupancies(int roomId ,[FromBody]DateDTO p)
        {
            var r = _patientRoomService.GetOccupancies(roomId, p);
            if (r == null) return NotFound("Room not found");
            if (!r.Any()) return NoContent();
            return Ok(r);
        }


        // PATCH: api/room/5
        [HttpPatch("{roomId:int}")]
        [ProducesResponseType(typeof(RoomDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult PatchRoom(int roomId, [FromBody]RoomDTO p)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            p.RoomId = roomId;

            var ret = _roomService.Update(p);
            if (ret != null)
            {
                return Ok(ret);
            }

            return NotFound("Room not found");
        }

        // DELETE: api/room/5
        [HttpDelete("{roomId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult DeleteRoom(int roomId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _roomService.Delete(roomId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound("Room not found");
        }
    }
}
