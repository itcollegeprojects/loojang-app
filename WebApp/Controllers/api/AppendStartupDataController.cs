﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/appendStartupData")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //[Authorize(Roles = "admin")]
    public class AppendStartupDataController : Controller
    {
        private readonly IAppendStartupDataService _appendStartupData;

        public AppendStartupDataController(IAppendStartupDataService appendStartupData)
        {
            _appendStartupData = appendStartupData;
        }

        // GET: api/appendStartupData
        [HttpGet]
        [ProducesResponseType(200)]
        [AllowAnonymous]
        public IActionResult GetAppendStartupData()
        {



            //_appendStartupData.AddRoom();

            //_appendStartupData.AddPatient();

            //_appendStartupData.AddPatientRoom();

            //_appendStartupData.AddPatientsGuardian();

            //_appendStartupData.AddPatientsDoctor();

            //_appendStartupData.AddMedicalcase();

            //_appendStartupData.AddPrescriptionWithProcedures();

            return Ok("done");

            /* delete all data: 
             * 
            delete from patients;
            delete from frequencytypes;
            delete from medicalcases;
            delete from patientrooms;
            delete from patientsdoctors;
            delete from patientsguardians;
            delete from prescriptions;
            delete from procedures;
            delete from rooms;

            delete from aspnetroleclaims;
            delete from aspnetroles;
            delete from aspnetusercaims;
            delete from aspnetuserlogins;
            delete from aspnetuserroles;
            delete from aspnetusers;
            delete from aspnetusertokens;

            */
        }

    }
}
