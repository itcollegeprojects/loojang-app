﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/procedures")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProceduresController : Controller
    {
        private readonly IProcedureService _procedureService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUserService _applicationUserService;

        public ProceduresController(IProcedureService procedureService, Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> userManager, IApplicationUserService applicationUserService)
        {
            _procedureService = procedureService;
            _userManager = userManager;
            _applicationUserService = applicationUserService;
        }


        // POST: api/procedures
        [HttpPost]
        [ProducesResponseType(typeof(ProcedureDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult Addprocedure([FromBody]ProcedureDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();
            try
            {
                var ret = _procedureService.Add(p);
                return Ok(ret);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }

        }
        
        // GET: api/procedures/5
        [HttpGet("{procedureId:int}")]
        [ProducesResponseType(typeof(ProcedureDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> Get(int procedureId)
        {
            ProcedureDTO r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _procedureService.Find(procedureId);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                    r = _procedureService.Find(procedureId, user.ApplicationUserId);
                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
            if (r == null) return NotFound("Procedure not found");
            return Ok(r);
        }



        // DELETE: api/procedures/5
        [HttpDelete("{procedureId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Delete(int procedureId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _procedureService.Delete(procedureId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // PATCH: api/procedures/5
        [HttpPatch("{procedureId:int}")]
        [ProducesResponseType(typeof(ProcedureDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "EmployeeOnly")]
        public IActionResult Patch(int procedureId, [FromBody]ProcedureDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            p.ProcedureId = procedureId;

            try
            {
                var ret = _procedureService.Update(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/procedures
        //[HttpGet("{startDate:DateTime}.{endDate:DateTime}.{onlyactive:length(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProcedureDTO>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> Get(DateTime startDate, DateTime endDate, string onlyactive = "f")
        {
            IEnumerable<ProcedureDTO> r;

            if (startDate == DateTime.MinValue & endDate == DateTime.MinValue & onlyactive == "f")
            {
                if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
                {
                    var id = User.Identity.GetUserId();
                    var u =  await _userManager.FindByEmailAsync(id);
                    var user = _applicationUserService.Find(u.Id);
                    r = _procedureService.All(user.ApplicationUserId);
                }
                else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
                {
                    r = _procedureService.All();
                }
                else
                {
                    return Unauthorized();
                }
                return Ok(r);
            }
            else if (onlyactive == "t")
            {
                if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
                {
                    var id = User.Identity.GetUserId();
                    var u = await _userManager.FindByEmailAsync(id);
                    var user = _applicationUserService.Find(u.Id);
                    r = _procedureService.FindByApplicationUserIdAndScheduledDate(user.ApplicationUserId,DateTime.MinValue, null);
                }
                else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
                {
                    r = _procedureService.FindByScheduledDate(DateTime.MinValue, null, onlyactive);
                }
                else
                {
                    return Unauthorized();
                }
                //return objects without enddate
                return Ok(r);
            }
            else
            {
                if (endDate < startDate & endDate != DateTime.MinValue)
                {
                    return NotFound();
                }
                if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
                {
                    var id = User.Identity.GetUserId();
                    var u = await _userManager.FindByEmailAsync(id);
                    var user = _applicationUserService.Find(u.Id);
                    r = _procedureService.FindByApplicationUserIdAndScheduledDate(user.ApplicationUserId, DateTime.MinValue, null);
                }
                else if (User.IsInRole("Doctor") || User.IsInRole("doctor") || User.IsInRole("Admin") || User.IsInRole("admin") || User.IsInRole("Caretaker") || User.IsInRole("caretaker"))
                {
                    r = _procedureService.FindByScheduledDate(startDate, endDate);
                }
                else
                {
                    return Unauthorized();
                }

                //return objects between dates
                return Ok(r);
            }

        }
    }
}
