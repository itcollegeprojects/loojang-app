﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using WebApp.Loggers;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/prescriptions")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PrescriptionsController : Controller
    {
        private readonly IPrescriptionService _prescriptionService;
        private readonly IProcedureService _procedureService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUserService _applicationUserService;
        private readonly ILogger _logger;

        public PrescriptionsController(IPrescriptionService prescriptionService, IProcedureService procedureService, Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> userManager, IApplicationUserService applicationUserService, ILogger<PrescriptionsController> logger)
        {
            _prescriptionService = prescriptionService;
            _procedureService = procedureService;
            _userManager = userManager;
            _applicationUserService = applicationUserService;
            _logger = logger;
        }

        // POST: api/prescriptions
        [HttpPost]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddPrescription([FromBody]PrescriptionDTO p)
        {
            if (!ModelState.IsValid) return BadRequest("Bad prescription model!");
            
            try
            {
                var ret = _prescriptionService.Add(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }

        }

        // POST: api/PrescriptionFrequencyProcedure
        [HttpPost("withprocedures")]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddWithProcedures([FromBody]PrescriptionDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();
            try
            {
                var ret = _prescriptionService.AddWithProcedures(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }


        // GET: api/prescriptions/5
        [HttpGet("{prescriptionId:int}")]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> Get(int prescriptionId)
        {
            PrescriptionDTO r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _prescriptionService.Find(prescriptionId);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                    r = _prescriptionService.Find(prescriptionId, user.ApplicationUserId);

                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
            if (r == null) return NotFound("Prescription room not found");
            return Ok(r);
        }

        // DELETE: api/prescriptions/5
        [HttpDelete("{prescriptionId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy="DoctorOnly")]
        public IActionResult Delete(int prescriptionId)
        {
            _logger.LogInformation(LoggingEvents.DeleteItem, "Delete item");
            if (!ModelState.IsValid) return BadRequest(ModelState);            

            var ret = _prescriptionService.Delete(prescriptionId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // PATCH: api/prescriptions/5
        [HttpPatch("{prescriptionId:int}")]
        [ProducesResponseType(typeof(PrescriptionDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult Patch(int prescriptionId, [FromBody]PrescriptionDTO p)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);

            p.PrescriptionId = prescriptionId;

            try
            {
                var ret = _prescriptionService.Update(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/prescriptions
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PrescriptionDTO>), 200)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> Get()
        {

            IEnumerable<PrescriptionDTO> r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _prescriptionService.All();
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);
                r = _prescriptionService.All(user.ApplicationUserId);
            }
            else
            {
                return Unauthorized();
            }

            return Ok(r);
        }

        // GET: api/prescriptions/5/procedures
        [HttpGet("{prescriptionId:int}/procedures")]
        [ProducesResponseType(typeof(IEnumerable<ProcedureDTO>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(204)]
        [Authorize(Policy = "UserWithRole")]
        public async Task<IActionResult> GetProcedures(int prescriptionId, string onlyactive = "f")
        {
            IEnumerable<ProcedureDTO> r;

            if (User.IsInRole("Admin") || User.IsInRole("Doctor"))
            {
                r = _procedureService.GetProceduresByPrescriptionId(prescriptionId);
            }
            else if (User.IsInRole("Guardian"))
            {
                var id = User.Identity.GetUserId();
                var u = await _userManager.FindByEmailAsync(id);
                var user = _applicationUserService.Find(u.Id);

                try
                {
                   r = _procedureService.GetProceduresByPrescriptionId(prescriptionId, user.ApplicationUserId);

                }
                catch (HttpRequestException)
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

            if (r == null) return NotFound("Prescription not found");
            return Ok(r);

        }
    }
}
