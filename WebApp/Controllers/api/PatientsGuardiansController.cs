﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/patientsguardians")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PatientsGuardiansController : Controller
    {
        private readonly IPatientsGuardianService _patientsGuardianService;

        public PatientsGuardiansController(IPatientsGuardianService patientsGuardianService)
        {
            _patientsGuardianService = patientsGuardianService;
        }


        // POST: api/patientsguardians
        [HttpPost]
        [ProducesResponseType(typeof(PatientsGuardianDTO), 200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult AddpatientsGuardian([FromBody]PatientsGuardianDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                var ret = _patientsGuardianService.Add(p);
                return Ok(ret);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/patientsguardians/5
        [HttpGet("{patientsGuardianId:int}")]
        [ProducesResponseType(typeof(PatientsGuardianDTO), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "UserWithRole")]
        public IActionResult Get(int patientsGuardianId)
        {
            PatientsGuardianDTO r;

            if (User.IsInRole("Guardian") || User.IsInRole("guardian") || User.IsInRole("patient") || User.IsInRole("Patient"))
            {
#warning not implemented
                r = null;
            }
            else
            {
                r = _patientsGuardianService.Find(patientsGuardianId);
            }

            if (r == null) return NotFound();
            return Ok(r);
        }

        // DELETE: api/patientsguardians/5
        [HttpDelete("{patientsGuardianId:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Delete(int patientsGuardianId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ret = _patientsGuardianService.Delete(patientsGuardianId);

            if (ret != null)
            {
                return Ok();
            }

            return NotFound();
        }

        // PATCH: api/patientsguardians/5
        [HttpPatch("{patientsGuardianId:int}")]
        [ProducesResponseType(typeof(PatientsGuardianDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "DoctorOnly")]
        public IActionResult Patch(int patientsGuardianId, [FromBody]PatientsGuardianDTO p)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            p.PatientsGuardianId = patientsGuardianId;

            try
            {
                var ret = _patientsGuardianService.Update(p);
                if (ret != null)
                {
                    return Ok(ret);
                }
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }

            return NotFound("Patients guardian not found");

        }

        // GET: api/patientsguardians
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PatientsGuardianDTO>), 200)]
        [Authorize(Policy = "EmployeeOnly")]
        public IEnumerable<PatientsGuardianDTO> Get()
        {
            return _patientsGuardianService.All();
        }


    }
}
