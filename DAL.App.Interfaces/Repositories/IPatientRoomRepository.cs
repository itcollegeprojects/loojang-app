﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPatientRoomRepository : IRepository<PatientRoom>
    {
        PatientRoom FindPresentRoomByPatientId(int id);

        Task<PatientRoom> FindPresentRoomByPatientIdAsync(int id);

        IEnumerable<PatientRoom> FindByPatientId(int id);

        Task<IEnumerable<PatientRoom>> FindByPatientIdAsync(int id);

        IEnumerable<PatientRoom> FindByRoomId(int id);

        Task<IEnumerable<PatientRoom>> FindByRoomIdAsync(int id);

        IEnumerable<PatientRoom> RoomOccupanciesDuringTimePeriod(int roomId, DateTime fromDate, DateTime? toDate);

        Task<IEnumerable<PatientRoom>> RoomOccupanciesDuringTimePeriodAsync(int roomId, DateTime fromDate,
            DateTime? toDate);
        int RoomOccupanciesDuringTimePeriodCount(int roomId, DateTime fromDate, DateTime? toDate);

        IEnumerable<PatientRoom> FindByPatientIds(List<int> patientIds);
        bool Exists(params object[] id);
    }
}