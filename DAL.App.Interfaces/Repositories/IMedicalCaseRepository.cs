﻿using DAL.Interfaces.Repositories;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IMedicalCaseRepository : IRepository<MedicalCase>
    {
        IEnumerable<MedicalCase> AllRelated();

        MedicalCase FindByMedicalCaseId(int id);
        Task<MedicalCase> FindByMedicalCaseIdAsync(int id);


        IEnumerable<MedicalCase> FindByPatientId(int id);
        Task<IEnumerable<MedicalCase>> FindByPatientIdAsync(int id);

        IEnumerable<MedicalCase> FindByDoctorId(string id);
        Task<IEnumerable<MedicalCase>> FindByDoctorIdAsync(string id);

        IEnumerable<MedicalCase> FindByPatientsDoctorId(int id);

        Task<IEnumerable<MedicalCase>> FindByPatientsDoctorIdAsync(int id);

        bool Exists(int medicalCaseId);
        IEnumerable<MedicalCase> FindByPatientIds(List<int> patientIds);
    }
}