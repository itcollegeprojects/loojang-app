﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IRoomRepository : IRepository<Room>
    {
        IEnumerable<Room> GetFreeRooms();
        Task<IEnumerable<Room>> GetFreeRoomsAsync();
        bool Exists(int pRoomId);
        Room FindWithoutInclude(params object[] id);

        Task<Room> FindWithoutIncludeAsync(params object[] id);



    }
}