﻿using DAL.Interfaces.Repositories;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPrescriptionRepository : IRepository<Prescription>
    {

        Prescription Find(int id);
        IEnumerable<Prescription> FindByMedicalCaseId(int id);

        Task<IEnumerable<Prescription>> FindByMedicalCaseIdAsync(int id);
        bool Exists(int prescriptionId);
        IEnumerable<Prescription> FindByPatientIds(List<int> patientIds);
    }
}