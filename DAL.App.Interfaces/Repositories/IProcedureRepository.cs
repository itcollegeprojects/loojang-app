﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IProcedureRepository : IRepository<Procedure>
    {
        new IEnumerable<Procedure> All();

        new Task<IEnumerable<Procedure>> AllAsync();

        Procedure Find(int id);
        Task<Procedure> FindAsync(int id);

        IEnumerable<Procedure> FindByPrescriptionId(int id);

        Task<IEnumerable<Procedure>> FindByPrescriptionIdAsync(int id);

        IEnumerable<Procedure> FindBetweenDates(DateTime startDate, DateTime? endDate, string onlyactive ="f");

        Task<IEnumerable<Procedure>> FindBetweenDatesAsync(DateTime startDate, DateTime? endDate, string onlyactive = "f");

        IEnumerable<Procedure> FindByPrescriptionIdBetweenDates(int id,DateTime startDate, DateTime? endDate);

        Task<IEnumerable<Procedure>> FindByPrescriptionIdBetweenDatesAsync(int id,DateTime startDate, DateTime? endDate);
        bool Exists(int procedureId);
        IEnumerable<Procedure> FindByPatientIds(List<int> patientIds);
        IEnumerable<Procedure> FindByPatientIdBetweenDates(List<int> patientIds, DateTime startDate, DateTime? endDate, string onlyactive = "f");
    }
}