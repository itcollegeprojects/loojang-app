﻿using System.Collections.Generic;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
        bool Exists(string applicationUserId);

        ApplicationUser FindWithoutTracking(string id);
    }
}