﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPatientRepository : IRepository<Patient>
    {
        Patient FindWithDetails(params object[] id);
        Task<Patient> FindWithDetailsAsync(params object[] id);

        IEnumerable<Patient> AllWithApplicationUser();
        IEnumerable<Patient> AllPresentPatients();
        IEnumerable<Patient> AllByDoctorsId(string applicationUserId);
        IEnumerable<Patient> AllByGuardianId(string applicationUserId);

        IEnumerable<Patient> AllByApplicationUserId(string userId);
        bool Exists(params object[] id);

    }
}