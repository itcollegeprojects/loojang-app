﻿using DAL.Interfaces.Repositories;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPatientsDoctorRepository : IRepository<PatientsDoctor>
    {
        IEnumerable<PatientsDoctor> FindByPatientId(int id);

        Task<IEnumerable<PatientsDoctor>> FindByPatientIdAsync(int id);

        IEnumerable<PatientsDoctor> FindByApplicationUserId(string id);

        Task<IEnumerable<PatientsDoctor>> FindByApplicationUserIdAsync(string id);
        bool Exists(int patientsDoctorId);
    }
}