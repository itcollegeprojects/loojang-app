﻿using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IFrequencyTypeRepository : IRepository<FrequencyType>
    {
        bool Exists(int pFrequencyTypeId);
    }
}