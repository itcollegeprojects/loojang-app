﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPatientsGuardianRepository : IRepository<PatientsGuardian>
    {
        IEnumerable<PatientsGuardian> FindByGuardianId(string applicationUserId);

        bool IsGuardian(string applicationUserId, int patientId);

        IEnumerable<PatientsGuardian> FindByPatientId(int id);

        Task<IEnumerable<PatientsGuardian>> FindByPatientIdAsync(int id);

        IEnumerable<PatientsGuardian> FindByApplicationUserId(string id);

        Task<IEnumerable<PatientsGuardian>> FindByApplicationUserIdAsync(string id);
    }
}