﻿using System;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IApplicationUserRepository ApplicationUsers { get; }
        IFrequencyTypeRepository FrequencyTypes { get; }

        IMedicalCaseRepository MedicalCases { get; }

        IPatientRepository Patients { get;  }

        IPatientRoomRepository PatientRooms { get; }

        IPatientsDoctorRepository PatientDoctors { get; }

        IPatientsGuardianRepository PatientGuardians { get; }

        IPrescriptionRepository Prescriptions { get; }

        IProcedureRepository Procedures { get; }

        IRoomRepository Rooms { get; }

    }
}
