﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DAL.App.EF.Migrations
{
    public partial class FrequencyType_added_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Hour",
                table: "FrequencyTypes",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Weekday",
                table: "FrequencyTypes",
                maxLength: 300,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Hour",
                table: "FrequencyTypes");

            migrationBuilder.DropColumn(
                name: "Weekday",
                table: "FrequencyTypes");
        }
    }
}
