﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DAL.App.EF.Migrations
{
    public partial class Mig4FrequencyTypeNameTypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FrquencyTypeName",
                table: "FrequencyTypes",
                newName: "FrequencyTypeName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FrequencyTypeName",
                table: "FrequencyTypes",
                newName: "FrquencyTypeName");
        }
    }
}
