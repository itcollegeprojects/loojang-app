﻿using System;
using System.Threading.Tasks;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using DAL.Interfaces.Repositories;

namespace DAL.App.EF
{
    public class AppEFUnitOfWork : IAppUnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public AppEFUnitOfWork(IDataContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            _applicationDbContext = dataContext as ApplicationDbContext;
            if (_applicationDbContext == null)
            {
                throw new NullReferenceException("No EF dbcontext found in UOW");
            }
        }


        public IFrequencyTypeRepository FrequencyTypes => GetCustomRepository<IFrequencyTypeRepository>();
        public IMedicalCaseRepository MedicalCases => GetCustomRepository<IMedicalCaseRepository>();
        public IPatientRepository Patients => GetCustomRepository<IPatientRepository>();
        public IPatientRoomRepository PatientRooms => GetCustomRepository<IPatientRoomRepository>();
        public IPatientsDoctorRepository PatientDoctors => GetCustomRepository<IPatientsDoctorRepository>();
        public IPatientsGuardianRepository PatientGuardians => GetCustomRepository<IPatientsGuardianRepository>();
        public IPrescriptionRepository Prescriptions => GetCustomRepository<IPrescriptionRepository>();
        public IProcedureRepository Procedures => GetCustomRepository<IProcedureRepository>();
        public IRoomRepository Rooms => GetCustomRepository<IRoomRepository>();
        public IApplicationUserRepository ApplicationUsers => GetCustomRepository<IApplicationUserRepository>();



        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _applicationDbContext.SaveChangesAsync();
        }

        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetEntityRepository<TEntity>();
        }

        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class
        {
            return _repositoryProvider.GetCustomRepository<TRepositoryInterface>();
        }
    }
}
