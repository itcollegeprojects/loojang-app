﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFPatientRoomRepository : EFRepository<PatientRoom>, IPatientRoomRepository
    {
        public EFPatientRoomRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public PatientRoom FindPresentRoomByPatientId(int id)
        {
            return RepositoryDbSet
                .Where(p =>
                    p.PatientId == id && (p.ToDate == null || p.ToDate > DateTime.Now))
                .Include(p => p.Room)
                .FirstOrDefault();
        }

        public bool Exists(params object[] id)
        {
            return RepositoryDbSet.Any(p => p.PatientRoomId == (int)id[0]);
        }

        public Task<PatientRoom> FindPresentRoomByPatientIdAsync(int id)
        {
            return RepositoryDbSet
                .Where(p =>
                    p.PatientId == id && (p.ToDate == null || p.ToDate > DateTime.Now))
                .Include(p => p.Room)
                .FirstOrDefaultAsync();
        }

        public IEnumerable<PatientRoom> FindByPatientId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.PatientId == id)
                .Include(p => p.Room)
                .ToList();
        }

        public async Task<IEnumerable<PatientRoom>> FindByPatientIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.PatientId == id)
                .Include(p => p.Room)
                .ToListAsync();
        }

        public IEnumerable<PatientRoom> FindByRoomId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.RoomId == id)
                .Include(p => p.Patient)
                .ToList();
        }

        public async Task<IEnumerable<PatientRoom>> FindByRoomIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.RoomId == id)
                .Include(p => p.Patient)
                .ToListAsync();
        }

        public int RoomOccupanciesDuringTimePeriodCount(int roomId, DateTime fromDate, DateTime? toDate)
        {
            return RepositoryDbSet
                .Count(p => (p.FromDate <= toDate) && (fromDate < p.ToDate) && (p.ToDate < DateTime.MaxValue));
        }

        public IEnumerable<PatientRoom> FindByPatientIds(List<int> patientIds)
        {
            return RepositoryDbSet
                .Include(p => p.Patient)
                .Include(p => p.Room)
                .Where(p => patientIds.Contains(p.PatientId))                
                .ToList();
        }
        

        public IEnumerable<PatientRoom> RoomOccupanciesDuringTimePeriod(int roomId, DateTime fromDate, DateTime? toDate)
        {
            return RepositoryDbSet
                .Where(p => (p.FromDate <= toDate)  && (fromDate < p.ToDate) && (p.ToDate < DateTime.MaxValue))
                .Include(p => p.Room)
                .Include(p => p.Patient)
                .ToList();
        }

        public async Task<IEnumerable<PatientRoom>> RoomOccupanciesDuringTimePeriodAsync(int roomId, DateTime fromDate, DateTime? toDate)
        {
            return await RepositoryDbSet
                 .Where(p => (p.FromDate <= toDate) && (fromDate <= p.ToDate) && (p.ToDate < DateTime.MaxValue))
                 .Include(p => p.Room)
                 .ToListAsync();
        }
    }
}