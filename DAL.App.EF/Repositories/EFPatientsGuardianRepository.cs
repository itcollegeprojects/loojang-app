﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFPatientsGuardianRepository : EFRepository<PatientsGuardian>, IPatientsGuardianRepository
    {
        public EFPatientsGuardianRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public IEnumerable<PatientsGuardian> FindByGuardianId(string applicationUserId)
        {
            return RepositoryDbSet
                .Where(p => p.ApplicationUserId == applicationUserId)
                .ToList();
        }

        public bool IsGuardian(string applicationUserId, int patientId)
        {
            return RepositoryDbSet.Any(p => p.PatientId == patientId && p.ApplicationUserId == applicationUserId);
        }

        public IEnumerable<PatientsGuardian> FindByPatientId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.PatientId == id)
                .Include(p => p.ApplicationUser)
                .ToList();
        }

        public async Task<IEnumerable<PatientsGuardian>> FindByPatientIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.PatientId == id)
                .Include(p => p.ApplicationUser)
                .ToListAsync();
        }

        public IEnumerable<PatientsGuardian> FindByApplicationUserId(string id)
        {
            return RepositoryDbSet
                .Where(p => p.ApplicationUserId == id)
                .Include(p => p.ApplicationUser)
                .ToList();
        }



        public async Task<IEnumerable<PatientsGuardian>> FindByApplicationUserIdAsync(string id)
        {
            return await RepositoryDbSet
                .Where(p => p.ApplicationUserId == id)
                .Include(p => p.ApplicationUser)
                .ToListAsync();
        }

    }
}