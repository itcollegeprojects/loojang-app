﻿using System.Linq;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFFrequencyTypeRepository : EFRepository<FrequencyType>, IFrequencyTypeRepository
    {
        public EFFrequencyTypeRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public bool Exists(int frequencyTypeId)
        {
            return RepositoryDbSet
                .Any(r => r.FrequencyTypeId == frequencyTypeId);
        }
    }
}