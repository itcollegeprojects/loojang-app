﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;


namespace DAL.App.EF.Repositories
{
    public class EFMedicalCaseRepository : EFRepository<MedicalCase>, IMedicalCaseRepository
    {
        public EFMedicalCaseRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public MedicalCase FindByMedicalCaseId(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .FirstOrDefault(p => p.MedicalCaseId == id);
        }

        public async Task<MedicalCase> FindByMedicalCaseIdAsync(int id)
        {
            return await RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .FirstOrDefaultAsync(p => p.MedicalCaseId == id);
        }

        public IEnumerable<MedicalCase> AllRelated()
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .ToList();
        }

        public new async Task<IEnumerable<MedicalCase>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(p => p.Prescriptions)
                .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .ToListAsync();
        }

        public IEnumerable<MedicalCase> FindByPatientId(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctor.PatientId == id)
                .ToList();
        }

        public async Task<IEnumerable<MedicalCase>> FindByPatientIdAsync(int id)
        {
            return await RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctor.PatientId == id)
                .ToListAsync();
        }

        public IEnumerable<MedicalCase> FindByDoctorId(string id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctor.ApplicationUserId == id)
                .ToList();
        }

        public async Task<IEnumerable<MedicalCase>> FindByDoctorIdAsync(string id)
        {
            return await RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctor.ApplicationUserId == id)
                .ToListAsync();
        }

        public new MedicalCase Find(params object[] id)
        {
            return RepositoryDbSet
                .Where(p => p.MedicalCaseId == (int)id[0])
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)                
                .FirstOrDefault();
        }

        public new async Task<MedicalCase> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Where(p => p.MedicalCaseId == (int)id[0])
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .FirstOrDefaultAsync();
        }

        public IEnumerable<MedicalCase> FindByPatientsDoctorId(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctorId == id)
                .ToList();
        }

        public async Task<IEnumerable<MedicalCase>> FindByPatientsDoctorIdAsync(int id)
        {
            return await RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => p.PatientsDoctorId == id)
                .ToListAsync();
        }

        public bool Exists(int medicalCaseId)
        {
            return RepositoryDbSet
                .Any(p => p.MedicalCaseId == medicalCaseId);
        }

        public IEnumerable<MedicalCase> FindByPatientIds(List<int> patientIds)
        {
            return RepositoryDbSet
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor.Patient)
                .Include(p => p.PatientsDoctor.ApplicationUser)
                .Where(p => patientIds.Contains(p.PatientsDoctor.PatientId)).ToList();
        }

        public new MedicalCase Update(MedicalCase md)
        {
            md = RepositoryDbSet.Update(md).Entity;
            return RepositoryDbSet
                .Where(p => p.MedicalCaseId == md.MedicalCaseId)
                .Include(p => p.Prescriptions)
                    .ThenInclude(p => p.FrequencyType)
                .Include(p => p.PatientsDoctor)
                    .ThenInclude(pd => pd.Patient)
                .Include(p => p.PatientsDoctor)
                    .ThenInclude(pd => pd.ApplicationUser)
                .FirstOrDefault();
        }

    }
}
