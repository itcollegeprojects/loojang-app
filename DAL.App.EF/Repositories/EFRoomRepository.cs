﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFRoomRepository : EFRepository<Room>, IRoomRepository
    {
        public EFRoomRepository(DbContext dataContext) : base(dataContext)
        {
        }
        public new Room Find(params object[] id)
        {
            return RepositoryDbSet
                .Where(r => r.RoomId == (int)id[0])
                .Include(r => r.PatientRooms)
                .FirstOrDefault();
        }

        public new async Task<Room> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Where(r => r.RoomId == (int)id[0])
                .Include(r => r.PatientRooms)
                .FirstOrDefaultAsync();
        }

        public Room FindWithoutInclude(params object[] id)
        {
            return RepositoryDbSet
                .FirstOrDefault(r => r.RoomId == (int)id[0]);
        }

        public async Task<Room> FindWithoutIncludeAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Where(r => r.RoomId == (int)id[0])
                .FirstOrDefaultAsync();
        }

        public bool Exists(int id)
        {
            return RepositoryDbSet
                .Any(r => r.RoomId == id);
        }

        public new IEnumerable<Room> All()
        {
            return RepositoryDbSet
                .Include(r => r.PatientRooms)
                .ToList();
        }

        public new async Task<IEnumerable<Room>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(r => r.PatientRooms)
                .ToListAsync();
        }

        public IEnumerable<Room> GetFreeRooms()
        {
            return RepositoryDbSet
                    .Where(r => (r.Capacity - r.PatientRooms.Count(
                                        p => (
                                            p.FromDate < DateTime.Today &&
                                            (p.ToDate > DateTime.Today || p.ToDate == null) &&
                                            p.RoomId == r.RoomId)) > 0))
                    .Include(r => r.PatientRooms)
                    .ToList();
        }

        public Task<IEnumerable<Room>> GetFreeRoomsAsync()
        {
            throw new System.NotImplementedException();
        }

    }
}