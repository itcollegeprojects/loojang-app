﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFPatientsDoctorRepository : EFRepository<PatientsDoctor>, IPatientsDoctorRepository
    {
        public EFPatientsDoctorRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public IEnumerable<PatientsDoctor> FindByApplicationUserId(string id)
        {
            return RepositoryDbSet
                .Where(p => p.ApplicationUserId == id)
                .Include(p => p.ApplicationUser)
                .ToList();
        }

        public async Task<IEnumerable<PatientsDoctor>> FindByApplicationUserIdAsync(string id)
        {
            return await RepositoryDbSet
                .Where(p => p.ApplicationUserId == id)
                .Include(p => p.ApplicationUser)
                .ToListAsync();
        }

        public bool Exists(int patientsDoctorId)
        {
            return RepositoryDbSet
                .Any(p => p.PatientsDoctorId == patientsDoctorId);
        }

        public IEnumerable<PatientsDoctor> FindByPatientId(int id)
        {
            return RepositoryDbSet                
                .Where(p => p.PatientId == id)
                .Include(p => p.ApplicationUser)
                .ToList();
        }

        public async Task<IEnumerable<PatientsDoctor>> FindByPatientIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.PatientId == id)
                .Include(p => p.ApplicationUser)
                .ToListAsync();
        }
    }
}