﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFProcedureRepository : EFRepository<Procedure>, IProcedureRepository
    {
        public EFProcedureRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public new IEnumerable<Procedure> All()
        {
            return RepositoryDbSet
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .ToList();
        }

        public new async Task<IEnumerable<Procedure>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .ToListAsync();
        }

        public Procedure Find(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .FirstOrDefault(p => p.ProcedureId == id);
        }

        public Task<Procedure> FindAsync(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .FirstOrDefaultAsync(p => p.ProcedureId == id);
        }

        public IEnumerable<Procedure> FindByPrescriptionId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.PrescriptionId == id)
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .ToList();
        }

        public async Task<IEnumerable<Procedure>> FindByPrescriptionIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.PrescriptionId == id)
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .ToListAsync();
        }

        public IEnumerable<Procedure> FindBetweenDates(DateTime startDate, DateTime? endDate, string onlyactive = "f")
        {
            IEnumerable<Procedure> r;
            IQueryable<Procedure> repSet;

            if (endDate == null)
            {
                repSet = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate);
            } else
            {
                repSet = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.ScheduledDate <= endDate);
            }

            if (onlyactive == "t")
            {
                repSet = repSet
                    .Where(p => p.ActualDate == null);
            }

            r = repSet
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToList();

            return r;
        }

        public async Task<IEnumerable<Procedure>> FindBetweenDatesAsync(DateTime startDate, DateTime? endDate, string onlyactive = "f")
        {
            IEnumerable<Procedure> r;
            IQueryable<Procedure> repSet;

            if (endDate == null)
            {
                repSet = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate);
            }
            else
            {
                repSet = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.ScheduledDate <= endDate);
            }

            if (onlyactive == "t")
            {
                repSet = repSet
                    .Where(p => p.ActualDate == null);
            }

            r = await repSet
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToListAsync();

            return r;
        }

        public IEnumerable<Procedure> FindByPrescriptionIdBetweenDates(int id, DateTime startDate, DateTime? endDate)
        {
            IEnumerable<Procedure> r;

            if (endDate == null)
            {
                r = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.PrescriptionId==id)
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToList();
            }
            else
            {
                r = RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.ScheduledDate <= endDate & p.PrescriptionId == id)
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToList();
            }

            return r;
        }

        public async Task<IEnumerable<Procedure>> FindByPrescriptionIdBetweenDatesAsync(int id, DateTime startDate, DateTime? endDate)
        {
            IEnumerable<Procedure> r;

            if (endDate == null)
            {
                r = await RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.PrescriptionId==id)
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToListAsync();
            }
            else
            {
                r = await RepositoryDbSet
                    .Where(p => p.ScheduledDate >= startDate & p.ScheduledDate <= endDate & p.PrescriptionId == id)
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToListAsync();
            }

            return r;
        }

        public bool Exists(int procedureId)
        {
            return RepositoryDbSet.Any(p => p.ProcedureId == procedureId);
        }

        public IEnumerable<Procedure> FindByPatientIds(List<int> patientIds)
        {
            return RepositoryDbSet
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Where(p => patientIds.Contains(p.Prescription.MedicalCase.PatientsDoctor.Patient.PatientId))
                .Include(p => p.Prescription.MedicalCase)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                .Include(p => p.Caretaker)
                .ToList();
        }

        public IEnumerable<Procedure> FindByPatientIdBetweenDates(List<int> patientIds, DateTime startDate, DateTime? endDate, string onlyactive = "f")
        {
            IEnumerable<Procedure> r;
            IQueryable<Procedure> repSet;

            if (endDate == null)
            {
                repSet = RepositoryDbSet
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Where(p => p.ScheduledDate >= startDate & patientIds.Contains(p.Prescription.MedicalCase.PatientsDoctor.Patient.PatientId));

            }
            else
            {
                repSet = RepositoryDbSet
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Where(p => p.ScheduledDate >= startDate & p.ScheduledDate <= endDate & patientIds.Contains(p.Prescription.MedicalCase.PatientsDoctor.Patient.PatientId))
                    ;
            }

            if (onlyactive == "t")
            {
                repSet = repSet.Where(p => p.ActualDate == null);
            }

            r =  repSet
                    .Include(p => p.Prescription.MedicalCase)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.Patient)
                    .Include(p => p.Prescription.MedicalCase.PatientsDoctor.ApplicationUser)
                    .Include(p => p.Caretaker)
                    .ToList();

            return r;
        }
    }
}