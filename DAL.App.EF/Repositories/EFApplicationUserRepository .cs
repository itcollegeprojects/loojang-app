﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFApplicationUserRepository : EFRepository<ApplicationUser>, IApplicationUserRepository
    {

        public EFApplicationUserRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public new ApplicationUser Find(params object[] id)
        {
            return RepositoryDbSet
                .Find(id);
        }

        public ApplicationUser FindWithoutTracking(string id)
        {
            ApplicationUser r = RepositoryDbSet.AsNoTracking().FirstOrDefault(p => p.Id == id);
            return r;
        }

        public new async Task<ApplicationUser> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .FindAsync(id);
        }

        public bool Exists(string caretakerId)
        {
            return RepositoryDbSet.Any(p => p.Id == caretakerId);
        }



        public new IEnumerable<ApplicationUser> All()
        {
            return RepositoryDbSet.ToList();
        }

        public new async Task<IEnumerable<ApplicationUser>> AllAsync()
        {
            return await RepositoryDbSet.ToListAsync();
        }

    }
}