﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFPatientRepository : EFRepository<Patient>, IPatientRepository
    {
        public EFPatientRepository(DbContext dataContext) : base(dataContext)
        {
            RepositoryDbContext = dataContext;
            RepositoryDbSet = dataContext.Set<Patient>();
            if (RepositoryDbSet == null)
            {
                throw new ArgumentException("DBSet not found in dbcontext!");
            }
        }

        public bool Exists(params object[] id)
        {
            return RepositoryDbSet.Any(p => p.PatientId == (int)id[0]);
        }

        public Patient FindWithDetails(params object[] id)
        {
            return RepositoryDbSet.Include(p => p.PatientRooms).SingleOrDefault(p => p.PatientId == (int)id[0]); ;
        }

        public async Task<Patient> FindWithDetailsAsync(params object[] id)
        {
            return await RepositoryDbSet.Include(p => p.PatientRooms).SingleOrDefaultAsync(p => p.PatientId == (int)id[0]);
        }

        public IEnumerable<Patient> AllWithApplicationUser()
        {
            return RepositoryDbSet
                .Include(a => a.PatientsGuardians)
                .ThenInclude(c => c.ApplicationUser)
                .ToList();
        }

        public IEnumerable<Patient> AllByApplicationUserId(string userId)
        {
            return RepositoryDbSet
                .Include(a => a.PatientsGuardians)
                .ThenInclude(c => c.ApplicationUser)
                .Where(p => p.PatientsGuardians.Any(c => c.ApplicationUserId == userId))
                .ToList();
        }

        public IEnumerable<Patient> AllPresentPatients()
        {
            return RepositoryDbSet
                .Where(p => p.JoinedDate <= DateTime.Today && (p.LeftDate == null || p.LeftDate > DateTime.Today))
                .ToList();
        }

        public IEnumerable<Patient> AllByDoctorsId(string applicationUserId)
        {            
            return RepositoryDbSet
                .Where(p => p.PatientsDoctors.Any(item => item.ApplicationUserId == applicationUserId))
                .ToList();
        }

        public IEnumerable<Patient> AllByGuardianId(string applicationUserId)
        {
            return RepositoryDbSet
                .Where(p => p.PatientsGuardians.Any(item => item.ApplicationUserId == applicationUserId))
                .ToList();
        }
    }
}