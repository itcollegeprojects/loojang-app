﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFPrescriptionRepository : EFRepository<Prescription>, IPrescriptionRepository
    {
        public EFPrescriptionRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public Prescription Find(int id)
        {
            return RepositoryDbSet
                .Include(p => p.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.FrequencyType)
                .Include(p => p.Procedures).FirstOrDefault(p => p.PrescriptionId == id);
        }

        public IEnumerable<Prescription> FindByMedicalCaseId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.MedicalCaseId == id)
                //.Include(p => p.MedicalCase)
                .ToList();
        }

        public async Task<IEnumerable<Prescription>> FindByMedicalCaseIdAsync(int id)
        {
            return await RepositoryDbSet
                .Where(p => p.MedicalCaseId == id)
                //.Include(p => p.MedicalCase)
                .ToListAsync();
        }

        public bool Exists(int prescriptionId)
        {
            return RepositoryDbSet.Any(p => p.PrescriptionId == prescriptionId);
        }

        public IEnumerable<Prescription> FindByPatientIds(List<int> patientIds)
        {
            return RepositoryDbSet
                .Include(p => p.MedicalCase.PatientsDoctor.Patient)
                .Include(p => p.FrequencyType)
                .Include(p => p.Procedures)
                .Where(p => patientIds.Contains(p.MedicalCase.PatientsDoctor.Patient.PatientId)).ToList();
        }
    }
}