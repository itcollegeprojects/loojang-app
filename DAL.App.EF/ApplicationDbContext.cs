﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<FrequencyType> FrequencyTypes { get; set; }
        public DbSet<MedicalCase> MedicalCases { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientRoom> PatientRooms { get; set; }
        public DbSet<PatientsGuardian> PatientsGuardians { get; set; }
        public DbSet<PatientsDoctor> PatientsDoctors { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
        public DbSet<Room> Rooms { get; set; }
        // public DbSet<ApplicationUser> ApplicationUsers { get; set; }




        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}