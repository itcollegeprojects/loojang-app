﻿using System;
using System.Collections.Generic;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {
                {typeof(IFrequencyTypeRepository), (dataContext) => new EFFrequencyTypeRepository(dataContext as ApplicationDbContext) },
                {typeof(IMedicalCaseRepository), (dataContext) => new EFMedicalCaseRepository(dataContext as ApplicationDbContext) },
                {typeof(IPatientRepository), (dataContext) => new EFPatientRepository(dataContext as ApplicationDbContext) },
                {typeof(IPatientRoomRepository), (dataContext) => new EFPatientRoomRepository(dataContext as ApplicationDbContext) },
                {typeof(IPatientsDoctorRepository), (dataContext) => new EFPatientsDoctorRepository(dataContext as ApplicationDbContext) },
                {typeof(IPatientsGuardianRepository), (dataContext) => new EFPatientsGuardianRepository(dataContext as ApplicationDbContext) },
                {typeof(IPrescriptionRepository), (dataContext) => new EFPrescriptionRepository(dataContext as ApplicationDbContext) },
                {typeof(IProcedureRepository), (dataContext) => new EFProcedureRepository(dataContext as ApplicationDbContext) },
                {typeof(IRoomRepository), (dataContext) => new EFRoomRepository(dataContext as ApplicationDbContext) },
                {typeof(IApplicationUserRepository), (dataContext) => new EFApplicationUserRepository(dataContext as ApplicationDbContext) }
            };
        }

        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface),
                out Func<IDataContext, object> factory
            );
            return factory;
        }

        public Func<IDataContext, object> GetRepositoryFactoryForType<TEntity>() where TEntity : class
        {
            return GetCustomRepositoryFactory<TEntity>() ?? GetStandardRepositoryFactory<TEntity>();
        }

        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (dataContext) => new EFRepository<TEntity>(dataContext as ApplicationDbContext);
        }
    }
}