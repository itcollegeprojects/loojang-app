﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace BL.Factories
{
    public class ApplicationUserBriefFactory : IApplicationUserBriefFactory
    {
        public ApplicationUserBriefDTO Create(ApplicationUser au)
        {
            return new ApplicationUserBriefDTO()
            {
                ApplicationUserId = au.Id,
                FirstName = au.FirstName,
                LastName = au.LastName
            };
        }
    }
}