﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientsDoctorFactory : IPatientsDoctorFactory
    {
        public PatientsDoctorDTO Create(PatientsDoctor pd)
        {
            return new PatientsDoctorDTO()
            {
                PatientsDoctorId = pd.PatientsDoctorId,
                StartTime = pd.StartTime,
                EndTime = pd.EndTime,
                PatientId = pd.PatientId,
                ApplicationUserId = pd.ApplicationUserId
            };
        }

        public PatientsDoctor Create(PatientsDoctorDTO dto)
        {
            return new PatientsDoctor()
            {
                PatientsDoctorId = dto.PatientsDoctorId,
                StartTime = dto.StartTime,
                EndTime = dto.EndTime,
                PatientId = dto.PatientId,
                ApplicationUserId = dto.ApplicationUserId
            };
        }
    }
}