﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IRoomFactory
    {
        RoomDTO Create(Room r);

        Room Create(RoomDTO dto);
    }
}
