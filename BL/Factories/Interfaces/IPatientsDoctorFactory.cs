﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientsDoctorFactory
    {
        PatientsDoctorDTO Create(PatientsDoctor pd);

        PatientsDoctor Create(PatientsDoctorDTO dto);
    }
}
