﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientFactory
    {
        PatientDTO Create(Patient p);

        Patient Create(PatientDTO dto);
    }
}
