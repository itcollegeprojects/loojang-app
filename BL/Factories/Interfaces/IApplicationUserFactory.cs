﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IApplicationUserFactory
    {
        ApplicationUserDTO Create(ApplicationUser au);

        ApplicationUser Create(ApplicationUserDTO dto);
    }
}
