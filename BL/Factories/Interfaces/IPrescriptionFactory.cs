﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPrescriptionFactory
    {
        PrescriptionDTO Create(Prescription p);

        Prescription Create(PrescriptionDTO dto);
    }
}
