﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientBriefFactory
    {
        PatientBriefDTO Create(Patient p);

    }
}
