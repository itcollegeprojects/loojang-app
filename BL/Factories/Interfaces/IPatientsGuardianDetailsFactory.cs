﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientsGuardianDetailsFactory
    {
        PatientsGuardianDetailsDTO Create(PatientsGuardian pd);

        PatientsGuardian Create(PatientsGuardianDetailsDTO dto);
    }
}
