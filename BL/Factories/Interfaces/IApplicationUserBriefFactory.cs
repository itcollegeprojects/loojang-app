﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IApplicationUserBriefFactory
    {
        ApplicationUserBriefDTO Create(ApplicationUser au);

    }
}
