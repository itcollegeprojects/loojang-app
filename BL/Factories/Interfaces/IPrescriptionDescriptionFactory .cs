﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPrescriptionDescriptionFactory
    {
        PrescriptionDescriptionDTO Create(Prescription p);

    }
}
