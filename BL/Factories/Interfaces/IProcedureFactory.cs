﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IProcedureFactory
    {
        ProcedureDTO Create(Procedure p);

        Procedure Create(ProcedureDTO dto);
    }
}
