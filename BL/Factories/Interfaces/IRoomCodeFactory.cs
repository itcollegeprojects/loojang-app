﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IRoomCodeFactory
    {
        RoomCodeDTO Create(Room r);

    }
}
