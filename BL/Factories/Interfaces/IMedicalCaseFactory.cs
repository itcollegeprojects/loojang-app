﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IMedicalCaseFactory
    {
        MedicalCaseDTO Create(MedicalCase mc);

        MedicalCase Create(MedicalCaseDTO dto);
    }
}
