﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientsGuardianFactory
    {
        PatientsGuardianDTO Create(PatientsGuardian pg);

        PatientsGuardian Create(PatientsGuardianDTO dto);
    }
}
