﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IFrequencyTypeFactory
    {
        FrequencyTypeDTO Create(FrequencyType ft);

        FrequencyType Create(FrequencyTypeDTO dto);
    }
}
