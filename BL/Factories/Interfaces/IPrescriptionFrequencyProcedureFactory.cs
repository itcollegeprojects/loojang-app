﻿using BL.DTO;
using Domain;
using System.Collections.Generic;

namespace BL.Factories.Interfaces
{
    public interface IPrescriptionFrequencyProcedureFactory
    {
        
        PrescriptionDTO CreatePrescriptionDTO(PrescriptionFrequencyProcedureDTO p);

        Prescription CreatePrescription(PrescriptionFrequencyProcedureDTO dto);

        PrescriptionFrequencyProcedureDTO CreatePrescriptionFrequencyProcedure(PrescriptionDTO prescription, 
            List<ProcedureDTO> procedures);
    }
}