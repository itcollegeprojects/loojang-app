﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IRoomsPatientFactory
    {

        RoomsPatientDTO Create(PatientRoom p);
    }
}
