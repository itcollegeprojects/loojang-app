﻿using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPatientsDoctorDetailsFactory
    {
        PatientsDoctorDetailsDTO Create(PatientsDoctor pd);

        PatientsDoctor Create(PatientsDoctorDetailsDTO dto);

        PatientsDoctorDetailsDTO Create(PatientsDoctor pd, ApplicationUser user);
    }
}
