﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class RoomsPatientFactory: IRoomsPatientFactory
    {

        public RoomsPatientDTO Create(PatientRoom p)
        {
            return new RoomsPatientDTO()
            {
                PatientRoomId = p.PatientRoomId,
                FromDate = p.FromDate,
                ToDate = p.ToDate,
                PatientId = p.PatientId,
                FirstName = p.Patient.FirstName,
                LastName = p.Patient.LastName,
            };
        }

    }
}
