﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientToDoctorFactory : IPatientToDoctorFactory
    {
        public PatientToDoctorDTO Create(PatientsDoctor pd)
        {
            return new PatientToDoctorDTO()
            {
                ApplicationUserId = pd.ApplicationUserId,
                FromDate = pd.StartTime,
                ToDate = pd.EndTime,
                PatientId = pd.PatientId
            };
        }

        public PatientsDoctor Create(PatientToDoctorDTO dto)
        {
            return new PatientsDoctor()
            {
                ApplicationUserId = dto.ApplicationUserId,
                StartTime = dto.FromDate,
                EndTime = dto.ToDate,
                PatientId = dto.PatientId
            };
        }
    }
}