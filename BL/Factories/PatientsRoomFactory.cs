﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientsRoomFactory: IPatientsRoomFactory
    {

        public PatientsRoomDTO Create(PatientRoom p)
        {
            return new PatientsRoomDTO()
            {
                PatientRoomId = p.PatientRoomId,
                FromDate = p.FromDate,
                ToDate = p.ToDate,
                RoomId = p.RoomId,
                RoomCode = p.Room.RoomCode
            };
        }

    }
}
