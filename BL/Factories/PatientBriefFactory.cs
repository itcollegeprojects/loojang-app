﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientBriefFactory : IPatientBriefFactory
    {
        public PatientBriefDTO Create(Patient p)
        {
            return new PatientBriefDTO()
            {
                PatientId = p.PatientId,
                FirstName = p.FirstName,
                LastName = p.LastName,
            };
        }

    }
}