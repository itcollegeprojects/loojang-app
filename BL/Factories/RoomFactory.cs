﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class RoomFactory : IRoomFactory
    {
        public RoomDTO Create(Room r)
        {

            return new RoomDTO()
            {
                RoomId = r.RoomId,
                RoomCode = r.RoomCode,
                Capacity = r.Capacity,
                Comment = r.Comment,
                Free = r.Free
            };
        }

        public Room Create(RoomDTO dto)
        {
            return new Room()
            {
                RoomId = dto.RoomId,
                RoomCode = dto.RoomCode,
                Capacity = dto.Capacity,
                Comment = dto.Comment,
            };
        }
    }
}