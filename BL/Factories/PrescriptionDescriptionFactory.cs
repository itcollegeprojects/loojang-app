﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PrescriptionDescriptionFactory : IPrescriptionDescriptionFactory
    {
        public PrescriptionDescriptionDTO Create(Prescription p)
        {
            return new PrescriptionDescriptionDTO()
            {
                PrescriptionId = p.PrescriptionId,
                Description = p.Description,
                StartDate = p.StartDate,
                EndDate = p.EndDate,
                FrequencyTypeId = p.FrequencyTypeId,
                FrequencyTypeName = p.FrequencyType.FrequencyTypeName,
                Comment = p.Comment

            };
        }
    }
}