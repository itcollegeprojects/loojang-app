﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace BL.Factories
{
    public class ApplicationUserFactory : IApplicationUserFactory
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUserFactory(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public ApplicationUserDTO Create(ApplicationUser au)
        {
            return new ApplicationUserDTO()
            {
                ApplicationUserId = au.Id,
                FirstName = au.FirstName,
                LastName = au.LastName,
                Email = au.Email,
                Roles = _userManager.GetRolesAsync(au).Result
            };
        }

        public ApplicationUser Create(ApplicationUserDTO dto)
        {
            return new ApplicationUser()
            {
                Id = dto.ApplicationUserId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
            };
        }
    }
}