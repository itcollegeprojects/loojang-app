﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    class PrescriptionFrequencyProcedureFactory : IPrescriptionFrequencyProcedureFactory
    {
        public Prescription CreatePrescription(PrescriptionFrequencyProcedureDTO dto)
        {
            return new Prescription()
            {
                PrescriptionId = dto.PrescriptionId,
                Description = dto.Description,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                FrequencyTypeId = dto.FrequencyTypeId,
                MedicalCaseId = dto.MedicalCaseId,
            };
        }

        public PrescriptionDTO CreatePrescriptionDTO(PrescriptionFrequencyProcedureDTO dto)
        {
            return new PrescriptionDTO()
            {
                PrescriptionId = dto.PrescriptionId,
                Description = dto.Description,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                FrequencyTypeId = dto.FrequencyTypeId,
                MedicalCaseId = dto.MedicalCaseId,
            };
        }

        public PrescriptionFrequencyProcedureDTO CreatePrescriptionFrequencyProcedure(PrescriptionDTO prescription,
            List<ProcedureDTO> procedures)
        {
            return new PrescriptionFrequencyProcedureDTO()
            {
                PrescriptionId = prescription.PrescriptionId,
                Description = prescription.Description,
                StartDate = prescription.StartDate,
                EndDate = prescription.EndDate,
                FrequencyTypeId = prescription.FrequencyTypeId,
                MedicalCaseId = prescription.MedicalCaseId,
                Procedures = procedures

            };
        }
    }
}
