﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class ProcedureFactory : IProcedureFactory
    {
        public ProcedureDTO Create(Procedure p)
        {            
            return new ProcedureDTO()
            {
                ProcedureId = p.ProcedureId,
                ScheduledDate = p.ScheduledDate,
                ActualDate = p.ActualDate,
                PrescriptionId = p.PrescriptionId,
                PrescriptionDescription = p.Prescription.Description,
                CaretakerId = p.CaretakerId,
                CaretakerName = p.CaretakerId == null ? null : p.Caretaker.FirstLastName,
                PatientId = p.Prescription.MedicalCase.PatientsDoctor.Patient.PatientId,
                PatientName = p.Prescription.MedicalCase.PatientsDoctor.Patient.FirstLastName,
                Comment = p.Comment
            };
        }

        public Procedure Create(ProcedureDTO dto)
        {
            return new Procedure()
            {
                ProcedureId = dto.ProcedureId,
                ScheduledDate = dto.ScheduledDate,
                ActualDate = dto.ActualDate,
                PrescriptionId = dto.PrescriptionId,
                CaretakerId = dto.CaretakerId,
                Comment = dto.Comment
            };
        }
    }
}