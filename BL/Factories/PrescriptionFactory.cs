﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PrescriptionFactory : IPrescriptionFactory
    {
        public PrescriptionDTO Create(Prescription p)
        {
            return new PrescriptionDTO()
            {
                PrescriptionId = p.PrescriptionId,
                Description = p.Description,
                StartDate = p.StartDate,
                EndDate = p.EndDate,
                FrequencyTypeId = p.FrequencyTypeId,
                MedicalCaseId = p.MedicalCaseId,
                Comment = p.Comment
            };
        }

        public Prescription Create(PrescriptionDTO dto)
        {
            return new Prescription()
            {
                PrescriptionId = dto.PrescriptionId,
                Description = dto.Description,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                FrequencyTypeId = dto.FrequencyTypeId,
                MedicalCaseId = dto.MedicalCaseId,
                Comment = dto.Comment
            };
        }
    }
}