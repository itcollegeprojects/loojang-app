﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class RoomCodeFactory : IRoomCodeFactory
    {
        public RoomCodeDTO Create(Room r)
        {

            return new RoomCodeDTO()
            {
                RoomId = r.RoomId,
                RoomCode = r.RoomCode,
                Free = r.Free
            };
        }

    }
}