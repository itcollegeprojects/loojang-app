﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientToRoomFactory: IPatientToRoomFactory
    {
        public PatientToRoomDTO Create(PatientRoom p)
        {
            return new PatientToRoomDTO()
            {
                PatientRoomId = p.PatientRoomId,
                FromDate = p.FromDate.Date,
                ToDate = p.ToDate?.Date,
                RoomId = p.RoomId,
                PatientId = p.PatientId
            };
        }

        public PatientRoom Create(PatientToRoomDTO dto)
        {
            return new PatientRoom()
            {
                PatientRoomId = dto.PatientRoomId,
                FromDate = dto.FromDate,
                ToDate = dto.ToDate,
                RoomId = dto.RoomId,
                PatientId = dto.PatientId
            };
        }
    }
}
