﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientsGuardianFactory : IPatientsGuardianFactory
    {
        public PatientsGuardianDTO Create(PatientsGuardian p)
        {
            return new PatientsGuardianDTO()
            {
                PatientsGuardianId = p.PatientsGuardianId,
                StartDate = p.StartDate,
                EndDate = p.EndDate,
                PatientId = p.PatientId,
                ApplicationUserId = p.ApplicationUserId
                
            };
        }

        public PatientsGuardian Create(PatientsGuardianDTO dto)
        {
            return new PatientsGuardian()
            {
                PatientsGuardianId = dto.PatientsGuardianId,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                PatientId = dto.PatientId,
                ApplicationUserId = dto.ApplicationUserId
            };
        }
    }
}