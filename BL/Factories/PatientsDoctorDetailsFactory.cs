﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientsDoctorDetailsFactory : IPatientsDoctorDetailsFactory
    {
        public PatientsDoctorDetailsDTO Create(PatientsDoctor pd)
        {
            return new PatientsDoctorDetailsDTO()
            {
                PatientsDoctorId = pd.PatientsDoctorId,
                PatientId = pd.PatientId,
                FromDate = pd.StartTime,
                ToDate = pd.EndTime,
                FirstName = pd.ApplicationUser.FirstName,
                LastName = pd.ApplicationUser.LastName,
                ApplicationUserId = pd.ApplicationUserId
            };
        }

        public PatientsDoctor Create(PatientsDoctorDetailsDTO dto)
        {
            return new PatientsDoctor()
            {
                PatientsDoctorId = dto.PatientsDoctorId,
                StartTime = dto.FromDate,
                EndTime = dto.ToDate,
                PatientId = dto.PatientId,
                ApplicationUserId = dto.ApplicationUserId,
            };
        }

        public PatientsDoctorDetailsDTO Create(PatientsDoctor pd, ApplicationUser user)
        {
            return new PatientsDoctorDetailsDTO()
            {
                PatientsDoctorId = pd.PatientsDoctorId,
                PatientId = pd.PatientId,
                FromDate = pd.StartTime,
                ToDate = pd.EndTime,
                FirstName = user.FirstName,
                LastName = user.LastName,
                ApplicationUserId = user.Id
            };
        }
    }
}