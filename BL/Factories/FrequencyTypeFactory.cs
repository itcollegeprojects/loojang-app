﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class FrequencyTypeFactory : IFrequencyTypeFactory
    {
        public FrequencyTypeDTO Create(FrequencyType ft)
        {
            return new FrequencyTypeDTO()
            {
                FrequencyTypeId = ft.FrequencyTypeId,
                FrequencyTypeName = ft.FrequencyTypeName,
                Comment = ft.Comment,
                Hour = ft.Hour,
                Weekday = ft.Weekday,
            };
        }

        public FrequencyType Create(FrequencyTypeDTO dto)
        {
            return new FrequencyType()
            {
                FrequencyTypeId = dto.FrequencyTypeId,
                FrequencyTypeName = dto.FrequencyTypeName,
                Comment = dto.Comment,
                Hour=dto.Hour,
                Weekday=dto.Weekday,
            };
        }
    }
}