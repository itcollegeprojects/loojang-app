﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class MedicalCaseFactory : IMedicalCaseFactory
    {
        public MedicalCaseDTO Create(MedicalCase mc)
        {
            PrescriptionDescriptionFactory prescriptionDescriptionFactory = new PrescriptionDescriptionFactory();
            return new MedicalCaseDTO()
            {
                MedicalCaseId = mc.MedicalCaseId,
                CreatedOn = mc.CreatedOn,
                Comment = mc.Comment,
                PatientsDoctorId = mc.PatientsDoctorId,
                PatientId = mc.PatientsDoctor.Patient.PatientId,
                PatientName = mc.PatientsDoctor.Patient.FirstLastName,
                DoctorId = mc.PatientsDoctor.ApplicationUserId,
                DoctorName = mc.PatientsDoctor.ApplicationUser.FirstLastName,

                Prescriptions = mc.Prescriptions?.Select(p => prescriptionDescriptionFactory
                    .Create(p)).ToList() ?? new List<PrescriptionDescriptionDTO>()
            };
        }

        public MedicalCase Create(MedicalCaseDTO dto)
        {
            return new MedicalCase()
            {
                MedicalCaseId = dto.MedicalCaseId,
                CreatedOn = (dto.CreatedOn != null) ? dto.CreatedOn : DateTime.Now,
                Comment = dto.Comment,
                PatientsDoctorId = dto.PatientsDoctorId,
            };
        }
    }
}