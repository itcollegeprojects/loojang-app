﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientFactory : IPatientFactory
    {
        public PatientDTO Create(Patient p)
        {
            return new PatientDTO()
            {
                PatientId = p.PatientId,
                PersonalCode = p.PersonalCode,
                FirstName = p.FirstName,
                LastName = p.LastName,
                BirthDay = p.BirthDay,
                JoinedDate = p.JoinedDate,
                LeftDate = p.LeftDate,
                Comment = p.Comment,
            };
        }

        public Patient Create(PatientDTO dto)
        {
            return new Patient()
            {
                PatientId = dto.PatientId,
                PersonalCode = dto.PersonalCode,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                BirthDay = dto.BirthDay,
                JoinedDate = dto.JoinedDate,
                LeftDate = dto.LeftDate,
                Comment = dto.Comment
            };
        }
    }
}