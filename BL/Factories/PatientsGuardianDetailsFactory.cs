﻿using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PatientsGuardianDetailsFactory : IPatientsGuardianDetailsFactory
    {
        public PatientsGuardianDetailsDTO Create(PatientsGuardian pg)
        {
            return new PatientsGuardianDetailsDTO()
            {
                PatientsGuardianId = pg.PatientsGuardianId,
                PatientId = pg.PatientId,
                FromDate = pg.StartDate,
                ToDate = pg.EndDate,
                FirstName = pg.ApplicationUser.FirstName,
                LastName = pg.ApplicationUser.LastName,
                ApplicationUserId = pg.ApplicationUserId
            };
        }

        public PatientsGuardian Create(PatientsGuardianDetailsDTO dto)
        {
            return new PatientsGuardian()
            {
                PatientsGuardianId = dto.PatientsGuardianId,
                StartDate = dto.FromDate,
                EndDate = dto.ToDate,
                PatientId = dto.PatientId,
                ApplicationUserId = dto.ApplicationUserId,
            };
        }
    }
}