﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class RoomDetailsDTO
    {
        public int RoomId { get; set; }

        public string RoomCode { get; set; }

        public string Comment { get; set; }

        public int Capacity { get; set; }

        public int Free { get; set; }

        public List<PatientBriefDTO> Patients { get; set; }

    }
}
