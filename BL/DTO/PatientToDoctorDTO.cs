﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PatientToDoctorDTO
    {
        public string ApplicationUserId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int PatientId { get; set; }
    }
}
