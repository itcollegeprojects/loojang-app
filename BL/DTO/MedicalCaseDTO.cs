﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class MedicalCaseDTO
    {
        public int MedicalCaseId { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(2000)]
        public string Comment { get; set; }

        [Required]
        public int PatientsDoctorId { get; set; }

        public int PatientId { get; set; }

        public string PatientName { get; set; }

        public string DoctorId { get; set; }

        public string DoctorName { get; set; }

        public List<PrescriptionDescriptionDTO> Prescriptions { get; set; }

    }
}
