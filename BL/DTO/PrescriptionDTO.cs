﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PrescriptionDTO
    {
        public int PrescriptionId { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        [Required]
        public int FrequencyTypeId { get; set; }

        [Required]
        public int MedicalCaseId { get; set; }
    }
}
