﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class PrescriptionFrequencyProcedureDTO
    {

        //Reponse DTO, no validation
        public int PrescriptionId { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int FrequencyTypeId { get; set; }

        public int MedicalCaseId { get; set; }

        public List<ProcedureDTO> Procedures {get;set;}
    }
}
