﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class RegisterDTO
    {
        public string ApplicationUserId { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(128)]
        public string Email { get; set; }
        [Required]
        [StringLength(128)]
        public string Password { get; set; }
        [Required]
        [StringLength(128)]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(128)]
        public string Firstname  { get; set; }
        [Required]
        [StringLength(128)]
        public string LastName { get; set; }
    }
}
