﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class LoginResponseDTO
    {

        public String Token;
        public String Name;
        public String Email;
        public String Id;
        public List<String> Roles;
    }
}
