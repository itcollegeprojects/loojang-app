﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PatientsDoctorDTO
    {
        public int PatientsDoctorId { get; set; }
        [Required] [DataType(DataType.Date)] public DateTime StartTime { get; set; }
        [DataType(DataType.Date)] public DateTime? EndTime { get; set; }
        [Required] public int PatientId { get; set; }

        [Required] public String ApplicationUserId { get; set; }
    }
}
