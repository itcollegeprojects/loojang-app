﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class FrequencyTypeDTO
    {
        public int FrequencyTypeId { get; set; }
        [Required]
        [StringLength(100)]
        public string FrequencyTypeName { get; set; }

        [StringLength(300)]
        public string Comment { get; set; }
        [MaxLength(300)]
        public string Weekday { get; set; }
        [MaxLength(300)]
        public string Hour { get; set; }
    }
}
