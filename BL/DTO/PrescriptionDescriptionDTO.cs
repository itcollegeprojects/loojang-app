﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PrescriptionDescriptionDTO
    {
        public int PrescriptionId { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        public int FrequencyTypeId { get; set; }

        public string FrequencyTypeName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int MedicalCaseId { get; set; }
    }
}
