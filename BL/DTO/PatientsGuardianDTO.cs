﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PatientsGuardianDTO
    {
        public int PatientsGuardianId { get; set; }

        [Required]
        [DataType(DataType.Date)]

        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        [Required]
        public int PatientId { get; set; }

        [Required]
        public string ApplicationUserId { get; set; }
    }
}
