﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PatientToRoomDTO
    {
        public int PatientRoomId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }

        [Required]
        public int RoomId { get; set; }

        [Required]
        public int PatientId { get; set; }

    }
}
