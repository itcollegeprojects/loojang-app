﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class RoomDTO
    {
        public int RoomId { get; set; }

        [Required]
        [StringLength(10)]
        public string RoomCode { get; set; }

        [StringLength(2000)]
        public string Comment { get; set; }

        [Required]
        public int Capacity { get; set; }

        public int Free { get; set; }

    }
}
