﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class PatientWithDoctorsDTO
    {
        public string PersonalCode { get; set; }
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
