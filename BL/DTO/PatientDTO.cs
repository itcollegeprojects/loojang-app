﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class PatientDTO
    {
        [Required]
        [StringLength(20, MinimumLength = 11)]
        public string PersonalCode { get; set; }
        public int PatientId { get; set; }
        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(128)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        [DataType(DataType.Date)]
        public DateTime JoinedDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime? LeftDate { get; set; }
        [StringLength(2000)]
        public string Comment { get; set; }
        public string Room { get; set; }

    }
}
