﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class PatientBriefDTO
    {
        public string PersonalCode { get; set; }
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
