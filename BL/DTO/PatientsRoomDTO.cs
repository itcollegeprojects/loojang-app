﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class PatientsRoomDTO
    {
        public int PatientRoomId { get; set; }

        public int RoomId { get; set; }

        public string RoomCode { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
        
    }
}
