﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class RoomsPatientDTO
    {

        //Response DTO, no validation
        public int PatientRoomId { get; set; }

        public int PatientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
        
    }
}
