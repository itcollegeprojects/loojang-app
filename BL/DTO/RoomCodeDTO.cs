﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Factories.Interfaces;
using Domain;

namespace BL.DTO
{
    public class RoomCodeDTO
    {
        public int RoomId { get; set; }

        public string RoomCode { get; set; }

        public int Free { get; set; }



    }
}
