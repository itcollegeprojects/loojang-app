﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class ProcedureDTO
    {
        public int ProcedureId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]

        public DateTime ScheduledDate { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? ActualDate { get; set; }

        [Required]
        public int PrescriptionId { get; set; }

        public string PrescriptionDescription { get; set; }

        [StringLength(3000)]
        public string Comment { get; set; }

        [Required]
        public string CaretakerId { get; set; }
        public string CaretakerName { get; set; }

        public int PatientId { get; set; }
        public string PatientName { get; set; }

        
        

    }
}
