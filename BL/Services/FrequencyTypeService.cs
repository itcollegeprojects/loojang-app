﻿using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class FrequencyTypeService : IFrequencyTypeService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFrequencyTypeFactory _frequencyTypeFactory;

        public FrequencyTypeService(IAppUnitOfWork uow, IFrequencyTypeFactory frequencyTypeFactory)
        {
            _uow = uow;
            _frequencyTypeFactory = frequencyTypeFactory;
        }

        public IEnumerable<FrequencyTypeDTO> All()
        {
           
            return _uow.FrequencyTypes.All()
                    .Select(p => _frequencyTypeFactory
                    .Create(p)).ToList(); ;
        }

        public FrequencyTypeDTO Find(int id)
        {
            //Room r = _uow.Rooms.Find(id);
            FrequencyType frequencyType = _uow.FrequencyTypes.Find(id);

            if (frequencyType != null)
            {
                return _frequencyTypeFactory.Create(frequencyType);
            }

            return null;
        }

        public FrequencyTypeDTO Add(FrequencyTypeDTO p)
        {
            FrequencyType frequencyType = _frequencyTypeFactory.Create(p);
            _uow.FrequencyTypes.Add(frequencyType);
            _uow.SaveChanges();
            return _frequencyTypeFactory.Create(frequencyType);
        }

        public FrequencyTypeDTO Update(FrequencyTypeDTO p)
        {
            FrequencyType frequencyType = _frequencyTypeFactory.Create(p);
            _uow.FrequencyTypes.Update(frequencyType);
            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            return _frequencyTypeFactory.Create(frequencyType);
        }

        public FrequencyTypeDTO Delete(int id)
        {
            FrequencyType p = _uow.FrequencyTypes.Find(id);

            if (p != null)
            {
                _uow.FrequencyTypes.Remove(id);
                _uow.SaveChanges();
                return _frequencyTypeFactory.Create(p);
            }

            return null;
        }
    }
}
