﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;

namespace BL.Services
{
    public class AppendStartupDataService : IAppendStartupDataService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFrequencyTypeFactory _frequencyTypeFactory;
        private readonly IPatientFactory _patientFactory;
        private readonly IPatientsRoomFactory _patientsRoomFactory;
        private readonly IRoomFactory _roomFactory;
        private readonly IRoomCodeFactory _roomCodeFactory;
        private readonly IRoomsPatientFactory _roomsPatientFactory;
        private readonly IPatientService _patientService;
        private readonly IRoomService _roomService;
        private readonly IPrescriptionService _prescriptionService;

        public AppendStartupDataService(IAppUnitOfWork uow, 
            IFrequencyTypeFactory frequencyTypeFactory,
            IPatientFactory patientFactory,
            IPatientsRoomFactory patientsRoomFactory,
            IRoomFactory roomFactory,
            IRoomCodeFactory roomCodeFactory,
            IRoomsPatientFactory roomsPatientFactory,
            IPatientService patientService,
            IRoomService roomService,
            IPrescriptionService prescriptionService
            )
        {
            _uow = uow;
            _frequencyTypeFactory = frequencyTypeFactory;
            _patientFactory = patientFactory;
            _patientsRoomFactory = patientsRoomFactory;
            _roomFactory = roomFactory;
            _roomCodeFactory = roomCodeFactory;
            _roomsPatientFactory = roomsPatientFactory;
            _patientService = patientService;
            _roomService = roomService;
            _prescriptionService = prescriptionService;
        }

        public void AddFrequencyType()
        {

            FrequencyType frequencyType = new FrequencyType()
            {
                FrequencyTypeName = "1 kord päevas",
                Comment = "1 kord päevas peale sööki"
            };

            _uow.FrequencyTypes.Add(frequencyType);
            _uow.SaveChanges();
            var res =  _frequencyTypeFactory.Create(frequencyType);
            Console.WriteLine("added " + typeof(FrequencyType) + res.FrequencyTypeId);

            frequencyType = new FrequencyType()
            {
                FrequencyTypeName = "3 korda päevas",
                Comment = "3 korda päevas peale sööki"
            };

            _uow.FrequencyTypes.Add(frequencyType);
            _uow.SaveChanges();

        }

        public void AddMedicalCase()
        {
            throw new NotImplementedException();
        }

        public void AddPatient()
        {
            Patient element = new Patient()
            {
                FirstName = "Peeter",
                LastName = "Pakiraam",
                PersonalCode = "35001010000",
                BirthDay = new DateTime(1950, 1, 1),
                JoinedDate = new DateTime(2018, 1, 1),
                LeftDate = null,
                Comment = "tegeles rattaspordiga"
            };
            _uow.Patients.Add(element);
            _uow.SaveChanges();

            element = new Patient()
            {
                FirstName = "Heli",
                LastName = "Kopter",
                PersonalCode = "44001010000",
                BirthDay = new DateTime(1940, 1, 1),
                JoinedDate = new DateTime(2017, 1, 1),
                LeftDate = null,
                Comment = "kogemustega piloot"
            };
            _uow.Patients.Add(element);
            _uow.SaveChanges();

        }

        public void AddPatientRoom()
        {
            int patientId = _patientService.FindByPersonalCode("35001010000").First().PatientId;
            int roomId = _roomService.FindByRoomCode("201").First().RoomId; ;
  
            PatientRoom element = new PatientRoom()
            {
                PatientId = patientId,
                RoomId = roomId,
                FromDate = new DateTime(2018, 1, 1),
                ToDate = null
            };
            _uow.PatientRooms.Add(element);
            _uow.SaveChanges();


            patientId = _patientService.FindByPersonalCode("44001010000").First().PatientId;
            roomId = _roomService.FindByRoomCode("301").First().RoomId; ;
             element = new PatientRoom()
            {
                PatientId = patientId,
                RoomId = roomId,
                FromDate = new DateTime(2017, 1, 1),
                ToDate = null
            };
            _uow.PatientRooms.Add(element);
            _uow.SaveChanges();

        }

        public void  AddPatientsDoctor()
        {
            int patientId = _patientService.FindByPersonalCode("35001010000").First().PatientId;
            string doctorUserId = "5a5980fe-59d7-4419-8749-796d2e6ee46e"; //doctor@eesti.ee


            PatientsDoctor element = new PatientsDoctor()
            {
                StartTime = new DateTime(2018,01,01),
                PatientId = patientId,
                ApplicationUserId = doctorUserId
            };
            _uow.PatientDoctors.Add(element);
            _uow.SaveChanges();
        }

        public void AddMedicalcase()
        {
            int patientDoctorId = 2;

            MedicalCase element = new MedicalCase()
            {
                CreatedOn= new DateTime(2018,2,1),
                Comment = "nohu",
                PatientsDoctorId=patientDoctorId

            };

            _uow.MedicalCases.Add(element);
            _uow.SaveChanges();

        }

        public void AddPatientsGuardian()
        {
            int patientId = _patientService.FindByPersonalCode("35001010000").First().PatientId;

            PatientsGuardian patientsGuardian = new PatientsGuardian()
            {
                ApplicationUserId= "38921212-0cac-4780-9729-a7097af15749", //guardian
                StartDate = new DateTime(2018, 1, 1),
                PatientId = patientId
            };
            _uow.PatientGuardians.Add(patientsGuardian);

        }


        public void AddPrescriptionWithProcedures()
        {
            int medicalCaseId = 1;
            int frquencyTypeId = 15; 
            PrescriptionDTO element = new PrescriptionDTO()
            {
                Description = "tablett nohu jaoks",
                StartDate =new DateTime(2018,02,01),
                EndDate = new DateTime(2018,02,15),
                FrequencyTypeId = frquencyTypeId,
                MedicalCaseId = medicalCaseId
            };
            _prescriptionService.AddWithProcedures(element);
        }

        public void AddProcedure()
        {
            throw new NotImplementedException();
        }

        public void AddRoom()
        {
            Room element = new Room()
            {
                RoomCode = "201",
                Capacity = 2,
                Comment = "meeste tuba"
            };
            _uow.Rooms.Add(element);
            _uow.SaveChanges();

            element = new Room()
            {
                RoomCode = "301",
                Capacity = 2,
                Comment = "naiste tuba, merevaade"
            };
            _uow.Rooms.Add(element);
            _uow.SaveChanges();
        }


    }
}
