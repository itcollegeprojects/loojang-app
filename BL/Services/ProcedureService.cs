﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;

namespace BL.Services
{
    public class ProcedureService : IProcedureService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IProcedureFactory _procedureFactory;
        private readonly IFrequencyTypeService _frequencyTypeService;
        private readonly IApplicationUserService _applicationUserService;

        public ProcedureService(IAppUnitOfWork uow,
            IProcedureFactory procedureFactory,
            IFrequencyTypeService frequencyTypeService,
            IApplicationUserService applicationUserService)
        {
            _uow = uow;
            _procedureFactory = procedureFactory;
            _frequencyTypeService = frequencyTypeService;
            _applicationUserService = applicationUserService;
        }

        public IEnumerable<ProcedureDTO> All()
        {
            return _uow.Procedures.All()
                .Select(p => _procedureFactory.Create(p))
                .ToList(); ;
        }

        public IEnumerable<ProcedureDTO> All(string applicationUserId)
        {
            var patientIds = _uow.PatientGuardians
                .FindByGuardianId(applicationUserId)
                .Select(p => p.PatientId)
                .ToList();

            return _uow.Procedures.FindByPatientIds(patientIds)
                    .Select(p => _procedureFactory
                    .Create(p)).ToList(); ;
        }

        public ProcedureDTO Find(int id)
        {
            Procedure procedure = _uow.Procedures.Find(id);
            if (procedure == null) return null;
            return _procedureFactory.Create(procedure);
        }


        private void VerifyDTO(ProcedureDTO p)
        {
            if (!_uow.ApplicationUsers.Exists(p.CaretakerId)) throw new HttpRequestException("Caretaker does not exist");

            if (!_uow.Prescriptions.Exists(p.PrescriptionId)) throw new HttpRequestException("Prescription does not exist");

            if (_applicationUserService.IsInRole(p.CaretakerId, "caretaker")) throw new HttpRequestException("User is not a caretaker");

        }

        public ProcedureDTO Add(ProcedureDTO p)
        {

            VerifyDTO(p);

            Procedure procedure = _procedureFactory.Create(p);
            _uow.Procedures.Add(procedure);
            _uow.SaveChanges();
            return _procedureFactory.Create(procedure);
        }

        public ProcedureDTO Update(ProcedureDTO p)
        {
            VerifyDTO(p);

            Procedure procedure = _procedureFactory.Create(p);
            _uow.Procedures.Update(procedure);
            _uow.SaveChanges();
            procedure = _uow.Procedures.FindAsync(p.ProcedureId).Result;
            return _procedureFactory.Create(procedure);
        }

        public ProcedureDTO Delete(int id)
        {
            Procedure p = _uow.Procedures.Find(id);

            if (p != null)
            {
                _uow.Procedures.Remove(id);
                _uow.SaveChanges();
                return _procedureFactory.Create(p);
            }

            return null;
        }

        public IEnumerable<ProcedureDTO> FindByScheduledDate(DateTime startDate, DateTime? endDate, string onlyactive ="f")
        {

            return _uow.Procedures
                .FindBetweenDates(startDate, endDate,onlyactive)
                .Select((p) => _procedureFactory.Create(p))
                .ToList();
        }

        public IEnumerable<ProcedureDTO> FindByApplicationUserIdAndScheduledDate(string applicationUserId,DateTime startDate, DateTime? endDate, string onlyactive = "f")
        {
            var patientIds = _uow.PatientGuardians.FindByGuardianId(
                applicationUserId).Select(p => p.PatientId).ToList();

            return _uow.Procedures
                .FindByPatientIdBetweenDates(patientIds,startDate, endDate, onlyactive)
                .Select((p) => _procedureFactory.Create(p))
                .ToList();
        }



        public IEnumerable<ProcedureDTO> GetProceduresByPrescriptionId(int prescriptionId)
        {
            return _uow.Procedures
                .FindByPrescriptionId(prescriptionId)
                .Select((p) => _procedureFactory.Create(p))
                .ToList();
        }

        public IEnumerable<ProcedureDTO> FindByPrescriptionIdAndScheduledDate(int prescriptionId, DateTime startDate, DateTime? endDate)
        {
            return _uow.Procedures
                .FindByPrescriptionIdBetweenDates(prescriptionId, startDate, endDate)
                .Select((p) => _procedureFactory.Create(p))
                .ToList();
        }

        public IEnumerable<ProcedureDTO> GetProceduresByPrescriptionId(int prescriptionId, string applicationUserId)
        {
            Prescription prescription = _uow.Prescriptions.Find(prescriptionId);
            if (!_uow.Prescriptions.Exists(prescriptionId)) throw new HttpRequestException();
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, prescription.MedicalCase.PatientsDoctor.PatientId)) throw new HttpRequestException();

            return _uow.Procedures
                .FindByPrescriptionId(prescriptionId)
                .Select((p) => _procedureFactory.Create(p))
                .ToList();
        }

        public ProcedureDTO Find(int procedureId, string applicationUserId)
        {
            Procedure procedure = _uow.Procedures.Find(procedureId);
            if (!_uow.Procedures.Exists(procedureId)) throw new HttpRequestException();
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, procedure.Prescription.MedicalCase.PatientsDoctor.PatientId)) throw new HttpRequestException();
            return _procedureFactory.Create(procedure);
        }

        public List<ProcedureDTO> CreateProceduresForPrescription(PrescriptionDTO prescription)
        {
            DateTime startDate = prescription.StartDate;
            DateTime endDate;
            List<ProcedureDTO> procedures = new List<ProcedureDTO>();
            List<TimeSpan> timeSpan = new List<TimeSpan>();
            FrequencyTypeDTO frequencyType;
            DateTime tmpDate;
            string weekdayStr;
            string hourStr;
            int[] weekday;
            int[] hour;

            if (prescription.FrequencyTypeId == 0)
            {
                throw new MissingFieldException($"Prescription {prescription.FrequencyTypeId} does not have FrequencyTypeId to generate Procedures automatically");
            }

            frequencyType = _frequencyTypeService.Find(prescription.FrequencyTypeId);

            if (prescription.EndDate == null || prescription.EndDate == DateTime.MinValue)
            {
                endDate = startDate.AddDays(10);
            }
            else
            {
                endDate = (DateTime)prescription.EndDate;
            }

            //load weekday and hour arrays
            weekdayStr = frequencyType.Weekday;
            hourStr = frequencyType.Hour;

            //error checks
            if (weekdayStr == null)
            {
                throw new ArgumentNullException($"Frequency type weekday array is not defined, do not know how to generate procedures");
            }
            if (hourStr == null)
            {
                throw new ArgumentNullException($"Frequency type hour array is not defined, do not know how to generate procedures");
            }

            weekday = Array.ConvertAll(weekdayStr.Split(','), int.Parse);
            hour = Array.ConvertAll(hourStr.Split(','), int.Parse);

            //generate timespans 
            foreach(int h in hour)
            {
                timeSpan.Add(new TimeSpan(h, 0, 0));
            }

            for (DateTime dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                tmpDate = dt;

                //check if current date is part of allowed weekday array
                if(weekday.Contains((int)(tmpDate.DayOfWeek + 6) % 7))
                {
                    //and then generate procedures for that day timespans
                    foreach (TimeSpan ts in timeSpan)
                    {
                        Procedure procedure = new Procedure()
                        {
                            ScheduledDate = tmpDate.Add(ts),
                            PrescriptionId = prescription.PrescriptionId,
                            Comment = "generated automatically"
                        };
                        _uow.Procedures.Add(procedure);
                        _uow.SaveChanges();
                        procedures.Add(_procedureFactory.Create(procedure));
                    }
                }
            }
            return procedures;
        }

        /*
        public List<ProcedureDTO> CreateProceduresForPrescription(int prescriptionId)
        {
            return CreateProceduresForPrescription(_prescriptionService.Find(prescriptionId));
        }
        */
    }
}
