﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using BL.DTO;
using BL.Factories;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class PatientRoomService : IPatientRoomService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPatientToRoomFactory _patientToRoomFactory;
        private readonly IPatientsRoomFactory _patientsRoomFactory;
        private readonly IRoomsPatientFactory _roomsPatientFactory;


        public PatientRoomService(
            IAppUnitOfWork uow,
            IPatientToRoomFactory patientToRoomFactory,
            IRoomsPatientFactory roomsPatientFactory,
            IPatientsRoomFactory patientsRoomFactory)
        {
            _uow = uow;
            _patientToRoomFactory = patientToRoomFactory;
            _roomsPatientFactory = roomsPatientFactory;
            _patientsRoomFactory = patientsRoomFactory;
        }
        public IEnumerable<PatientToRoomDTO> All()
        {
            return _uow.PatientRooms.All()
                        .Select(p => _patientToRoomFactory.Create(p))
                        .ToList();
        }
        public IEnumerable<PatientToRoomDTO> All(string applicationUserId)
        {
            var patientIds = _uow.PatientGuardians.FindByGuardianId(
                applicationUserId).Select(p => p.PatientId).ToList();

            return _uow.PatientRooms
                .FindByPatientIds(patientIds)
                .Select((p) => _patientToRoomFactory.Create(p))
                .ToList();
        }

        public PatientToRoomDTO Find(int id)
        {
            PatientRoom patientRoom = _uow.PatientRooms.Find(id);
            return _patientToRoomFactory.Create(patientRoom);
        }

        public PatientToRoomDTO Find(int patientRoomId, string applicationUserId)
        {
            PatientRoom patientRoom = _uow.PatientRooms.Find(patientRoomId);
            if (!_uow.PatientRooms.Exists(patientRoomId)) throw new HttpRequestException();
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, patientRoom.PatientId)) throw new HttpRequestException();
            return _patientToRoomFactory.Create(patientRoom);
        }


        private bool RoomHasVacanciesDuringTimePeriod(Room room, DateTime fromDate, DateTime? toDate)
        {
            var occupancies = _uow.PatientRooms.RoomOccupanciesDuringTimePeriodCount(room.RoomId, fromDate, toDate);
            if (occupancies >= room.Capacity) return false;
            return true;
        }

        private bool PatientAlreadyInRoomDuringTimePeriod(Room room, DateTime fromDate, DateTime? toDate, int patientId)
        {
            var occupancies = _uow.PatientRooms.RoomOccupanciesDuringTimePeriod(room.RoomId, fromDate, toDate);

            if (!occupancies.Any())
            {
                Trace.WriteLine(message: "No occupancies for time period");
                return false;
            }

            var any = occupancies.Any(
                p => p.PatientId == patientId);

            return any;
        }

        private void VerifyDTO(PatientToRoomDTO p, Boolean update)
        {            
            // ToDate can't be earlier than FromDate
            if (p.ToDate?.Date < p.FromDate.Date) throw new HttpRequestException("ToDate can't be earlier than FromDate");

            if (!_uow.Rooms.Exists(p.RoomId)) throw new HttpRequestException("Room does not exist");

            if (!_uow.Patients.Exists(p.PatientId)) throw new HttpRequestException("Patient does not exist");

            var newRoom = _uow.Rooms.FindWithoutInclude(p.RoomId);

            // Check if the room even has vacancies during the specified period
            if (!RoomHasVacanciesDuringTimePeriod(newRoom, p.FromDate, p.ToDate)) throw new HttpRequestException("Room doesn't have vacancies during the time period");

            if (update) return;            
        }

        public PatientToRoomDTO AddPatientToRoom(PatientToRoomDTO p)
        {

            // If no ToDate is set, assume that patient is in the room indefinitely
            if (p.ToDate == null) p.ToDate = DateTime.MaxValue;

            VerifyDTO(p, false);

            Patient newPatient = _uow.Patients.Find(p.PatientId);
            LeavePreviousRooms(newPatient);

            PatientRoom patientRoom = _patientToRoomFactory.Create(p);
            _uow.PatientRooms.Add(patientRoom);
            _uow.SaveChanges();

            return _patientToRoomFactory.Create(patientRoom);
        }

        private void LeavePreviousRooms(Patient addedPatient)
        {
            foreach (PatientRoom pr in _uow.PatientRooms.FindByPatientId(addedPatient.PatientId))
            {
                if (pr.ToDate == null || pr.ToDate > DateTime.Today)
                {
                    pr.ToDate = DateTime.Now;
                    _uow.PatientRooms.Update(pr);
                }
            }
        }

        public PatientToRoomDTO Update(PatientToRoomDTO p)
        {
            VerifyDTO(p, true);
            PatientRoom patientRoom = _patientToRoomFactory.Create(p);
            _uow.PatientRooms.Update(patientRoom);

            try
            {

                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }
            return _patientToRoomFactory.Create(patientRoom);

        }

        public PatientToRoomDTO Delete(int id)
        {
            PatientRoom p = _uow.PatientRooms.Find(id);

            if (p != null)
            {
                _uow.PatientRooms.Remove(id);
                _uow.SaveChanges();
                return _patientToRoomFactory.Create(p);
            }

            return null;
        }

        public IEnumerable<RoomsPatientDTO> GetOccupancies(int roomId, DateDTO dateDto)
        {
            return _uow.PatientRooms
                .RoomOccupanciesDuringTimePeriod(roomId, dateDto.FromDate, dateDto.ToDate)
                .Select(p => _roomsPatientFactory.Create(p))
                .ToList(); ;
        }

        public IEnumerable<PatientsRoomDTO> FindByPatientId(int id)
        {
            var r = _uow.PatientRooms
                .FindByPatientId(id)
                .Select(p => _patientsRoomFactory.Create(p))
                .ToList();

            return r;
        }


    }
}
