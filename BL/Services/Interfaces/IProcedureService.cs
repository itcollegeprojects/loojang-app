﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Services.Interfaces
{
    public interface IProcedureService
    {

        IEnumerable<ProcedureDTO> All();  

        ProcedureDTO Find(int id);

        ProcedureDTO Add(ProcedureDTO p);

        ProcedureDTO Update(ProcedureDTO p);

        ProcedureDTO Delete(int id);
        IEnumerable<ProcedureDTO> FindByScheduledDate(DateTime startDate, DateTime? endDate, string onlyactive = "f");        
        List<ProcedureDTO> CreateProceduresForPrescription(PrescriptionDTO prescription);
        IEnumerable<ProcedureDTO> GetProceduresByPrescriptionId(int prescriptionId);
        IEnumerable<ProcedureDTO> FindByPrescriptionIdAndScheduledDate(int prescriptionId, DateTime startDate, DateTime? endDate);
        IEnumerable<ProcedureDTO> GetProceduresByPrescriptionId(int prescriptionId, string applicationUserId);

        ProcedureDTO Find(int procedureId, string applicationUserId);
        IEnumerable<ProcedureDTO> All(string applicationUserId);
        IEnumerable<ProcedureDTO> FindByApplicationUserIdAndScheduledDate(string applicationUserId,DateTime startDate, DateTime? endDate, string onlyactive = "f");
    }
}
