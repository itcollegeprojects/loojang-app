﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPatientsDoctorService
    {

        IEnumerable<PatientsDoctorDTO> All();  

        PatientsDoctorDTO Find(int id);

        PatientsDoctorDTO Add(PatientsDoctorDTO pd);

        PatientToDoctorDTO Add(PatientToDoctorDTO pd);

        PatientsDoctorDTO Update(PatientsDoctorDTO pd);

        PatientsDoctorDTO Delete(int id);

        IEnumerable<PatientsDoctorDetailsDTO> FindByPatientId(int patientId);

        IEnumerable<PatientsDoctorDetailsDTO> FindByApplicationUserId(string doctorApplicationUserId);

        IEnumerable<PatientsDoctorDetailsDTO> FindByApplicationUserIdAndPatientId(string applicationUserId, int patientId);

        PatientsDoctorDTO FindByApplicationUserIdAndId(string applicationUserId, int id);
    }
}
