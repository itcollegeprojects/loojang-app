﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IApplicationUserService
    {

        IEnumerable<ApplicationUserDTO> All();
        
        IEnumerable<ApplicationUserDTO> AllByRole(string role);        
        
        ApplicationUserDTO Find(string id);

        ApplicationUserDTO Add(ApplicationUserDTO au);

        ApplicationUserDTO AddToRole(string id, string newRole);

        Task<ApplicationUserDTO> RemoveFromRole(string id, string oldRole);

        ApplicationUserDTO Update(ApplicationUserDTO au);

        ApplicationUserDTO Delete(string id);
        bool IsInRole(string pCaretakerId, string caretaker);
    }
}

