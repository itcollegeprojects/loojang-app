﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPatientRoomService
    {

        IEnumerable<PatientToRoomDTO> All();

        IEnumerable<PatientToRoomDTO> All(string applicationUserId);

        PatientToRoomDTO Find(int id);

        PatientToRoomDTO AddPatientToRoom(PatientToRoomDTO p);

        PatientToRoomDTO Update(PatientToRoomDTO p);

        PatientToRoomDTO Delete(int id);

        IEnumerable<RoomsPatientDTO> GetOccupancies(int roomId, DateDTO dateDto);

        PatientToRoomDTO Find(int patientRoomId, string userApplicationUserId);

        IEnumerable<PatientsRoomDTO> FindByPatientId(int id);
    }
}
