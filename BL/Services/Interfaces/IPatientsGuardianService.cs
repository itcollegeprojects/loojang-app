﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPatientsGuardianService
    {

        IEnumerable<PatientsGuardianDTO> All();

        IEnumerable<PatientsGuardianDTO> FindByGuardianId(string applicationUserId);

        PatientsGuardianDTO Find(int id);

        PatientsGuardianDTO Add(PatientsGuardianDTO p);

        PatientsGuardianDTO Update(PatientsGuardianDTO p);

        PatientsGuardianDTO Delete(int id);

        IEnumerable<PatientsGuardianDetailsDTO> FindByPatientId(int patientId);

        IEnumerable<PatientsGuardianDetailsDTO> FindByApplicationUserIdAndPatientId(string applicationUserId, int patientId);
    }
}
