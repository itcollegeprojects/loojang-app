﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPrescriptionService
    {

        IEnumerable<PrescriptionDTO> All();  

        PrescriptionDTO Find(int id);

        PrescriptionDTO Add(PrescriptionDTO ps);

        PrescriptionFrequencyProcedureDTO AddWithProcedures(PrescriptionDTO ps);

        PrescriptionDTO Update(PrescriptionDTO ps);
        PrescriptionDTO Delete(int id);
        IEnumerable<PrescriptionDTO> FindPrescriptionsByMedicalCaseId(int medicalCaseId);
        IEnumerable<PrescriptionDescriptionDTO> FindPrescriptionDescriptionsByMedicalCaseId(int medicalCaseId);
        IEnumerable<PrescriptionDTO> FindPrescriptionsByMedicalCaseId(int medicalCaseId, string applicationUserId);
        IEnumerable<PrescriptionDTO> All(string userApplicationUserId);
        PrescriptionDTO Find(int prescriptionId, string applicationUserId);
    }
}
