﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IMedicalCaseService
    {
        IEnumerable<MedicalCaseDTO> All();
        MedicalCaseDTO Find(int id);
        MedicalCaseDTO Add(MedicalCaseDTO mc);
        MedicalCaseDTO Update(MedicalCaseDTO mc);
        MedicalCaseDTO Delete(int id);
        IEnumerable<MedicalCaseDTO> FindByPatientsDoctorsId(int patientsDoctorId);
        IEnumerable<MedicalCaseDTO> FindByDoctorId(string id);
        IEnumerable<MedicalCaseDTO> FindByPatientId(int id);
        IEnumerable<MedicalCaseDTO> AllByGuardianId(string userApplicationUserId);
        MedicalCaseDTO FindForGuardian(int id, string applicationUserId);
    }
}
