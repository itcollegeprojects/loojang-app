﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IAppendStartupDataService
    {
        void AddFrequencyType();
        void AddMedicalCase();
        void AddPatientRoom();

        void AddPatient();
        void AddPatientsGuardian();
        void AddPrescriptionWithProcedures();
        void AddProcedure();
        void AddRoom();
        void AddPatientsDoctor();
        void AddMedicalcase();
    }
}
