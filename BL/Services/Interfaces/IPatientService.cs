﻿using System.Collections;
using System.Collections.Generic;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPatientService
    {
        IEnumerable<PatientDTO> All();

        PatientDTO Find(int id);

        IEnumerable<PatientDTO> FindByPersonalCode(string personalcode);

        PatientDTO Add(PatientDTO p);

        PatientDTO Update(PatientDTO p);

        PatientDTO Delete(int id);

        IEnumerable<PatientsRoomDTO> GetRoomsByPatientId(int id);

        IEnumerable<PatientsDoctorDetailsDTO> GetDoctorsByPatientId(int id);

        IEnumerable<PatientsGuardianDetailsDTO> GetGuardiansByPatientId(int id);

        IEnumerable<PatientDTO> All(string applicationUserId);

        IEnumerable<PatientDTO> AllPresentPatients();

        IEnumerable<PatientDTO> AllByGuardianId(string applicationUserId);

        IEnumerable<PatientDTO> AllByDoctorsId(string applicationUserId);
    }
}