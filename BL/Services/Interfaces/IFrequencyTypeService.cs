﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IFrequencyTypeService
    {

        IEnumerable<FrequencyTypeDTO> All();

        FrequencyTypeDTO Find(int id);

        FrequencyTypeDTO Add(FrequencyTypeDTO ft);

        FrequencyTypeDTO Update(FrequencyTypeDTO ft);

        FrequencyTypeDTO Delete(int id);
    }
}
