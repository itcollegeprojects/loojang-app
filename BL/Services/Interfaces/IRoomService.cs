﻿using System.Collections;
using System.Collections.Generic;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IRoomService
    {
        IEnumerable<RoomDTO> All();

        RoomDTO Find(int id);

        IEnumerable<RoomDTO> FindByRoomCode(string code);

        RoomDTO Add(RoomDTO p);

        RoomDTO Update(RoomDTO p);

        RoomDTO Delete(int id);

        IEnumerable<RoomsPatientDTO> GetPatientsByRoomId(int roomId);

        IEnumerable<RoomCodeDTO> GetFreeRooms();
    }
}