﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using BL.DTO;
using BL.Factories;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class PrescriptionService : IPrescriptionService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPrescriptionFactory _prescriptionFactory;
        private readonly IPrescriptionDescriptionFactory _prescriptionDescriptionFactory;
        private readonly IProcedureFactory _procedureFactory;
        private readonly IPrescriptionFrequencyProcedureFactory _prescriptionFrequencyProcedureFactory;
        private readonly IProcedureService _procedureService;

        public PrescriptionService(IAppUnitOfWork uow,
            IPrescriptionFactory prescriptionFactory,
            IProcedureService procedureService, IPrescriptionDescriptionFactory prescriptionDescriptionFactory)
        {
            _uow = uow;
            _prescriptionFactory = prescriptionFactory;
            _prescriptionFrequencyProcedureFactory = new PrescriptionFrequencyProcedureFactory();
            _procedureService = procedureService;
            _prescriptionDescriptionFactory = prescriptionDescriptionFactory;
        }

        public IEnumerable<PrescriptionDTO> All()
        {
            return _uow.Prescriptions.All()
                    .Select(p => _prescriptionFactory
                    .Create(p)).ToList();
        }

        public PrescriptionDTO Find(int id)
        {
            Prescription prescription = _uow.Prescriptions.Find(id);
            if (prescription == null) return null;
            return _prescriptionFactory.Create(prescription);
        }

        public PrescriptionDTO Add(PrescriptionDTO p)
        {

            VerifyDTO(p);

            Prescription prescription = _prescriptionFactory.Create(p);
            _uow.Prescriptions.Add(prescription);
            _uow.SaveChanges();
            return _prescriptionFactory.Create(prescription);
        }

        private void VerifyDTO(PrescriptionDTO p)
        {
            if (!_uow.FrequencyTypes.Exists(p.FrequencyTypeId)) throw new HttpRequestException("Frequency type does not exist");

            if (!_uow.MedicalCases.Exists(p.MedicalCaseId)) throw new HttpRequestException("Medical case does not exist");

            if (p.EndDate < p.StartDate) throw new HttpRequestException("End date can not be before start date");
        }

        public PrescriptionDTO Update(PrescriptionDTO p)
        {

            VerifyDTO(p);

            Prescription prescription = _prescriptionFactory.Create(p);
            _uow.Prescriptions.Update(prescription);

            try
            {

                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }
            return _prescriptionFactory.Create(prescription);
        }

        public PrescriptionDTO Delete(int id)
        {
            Prescription p = _uow.Prescriptions.Find(id);

            if (p != null)
            {
                _uow.Prescriptions.Remove(id);
                _uow.SaveChanges();
                return _prescriptionFactory.Create(p);
            }

            return null;
        }

        public IEnumerable<PrescriptionDTO> FindPrescriptionsByMedicalCaseId(int medicalCaseId)
        {
            if (!_uow.MedicalCases.Exists(medicalCaseId)) return null;
            return _uow.Prescriptions
                .FindByMedicalCaseId(medicalCaseId)
                .Select((p) => _prescriptionFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PrescriptionDescriptionDTO> FindPrescriptionDescriptionsByMedicalCaseId(int medicalCaseId)
        {
            if (!_uow.MedicalCases.Exists(medicalCaseId)) return null;            
            return _uow.Prescriptions
                .FindByMedicalCaseId(medicalCaseId)
                .Select((p) => _prescriptionDescriptionFactory.Create(p))
                .ToList();
        }        

        public IEnumerable<PrescriptionDTO> FindPrescriptionsByMedicalCaseId(int medicalCaseId, string applicationUserId)
        {
            if (!_uow.MedicalCases.Exists(medicalCaseId)) return null;
            MedicalCase medicalCase = _uow.MedicalCases.FindByMedicalCaseId(medicalCaseId);
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, medicalCase.PatientsDoctor.PatientId)) throw new HttpRequestException();
            return _uow.Prescriptions
                .FindByMedicalCaseId(medicalCaseId)
                .Select((p) => _prescriptionFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PrescriptionDTO> All(string applicationUserId)
        {
            var patientIds = _uow.PatientGuardians.FindByGuardianId(
                applicationUserId).Select(p => p.PatientId).ToList();

            return _uow.Prescriptions
                .FindByPatientIds(patientIds)
                .Select((p) => _prescriptionFactory.Create(p))
                .ToList();
        }

        public PrescriptionDTO Find(int prescriptionId, string applicationUserId)
        {
            Prescription prescription = _uow.Prescriptions.Find(prescriptionId);
            if (!_uow.Prescriptions.Exists(prescriptionId)) throw new HttpRequestException();
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, prescription.MedicalCase.PatientsDoctor.PatientId)) throw new HttpRequestException();
            return _prescriptionFactory.Create(prescription);
        }

        public PrescriptionFrequencyProcedureDTO AddWithProcedures(PrescriptionDTO ps)
        {
            Prescription prescription = _prescriptionFactory.Create(ps);
            _uow.Prescriptions.Add(prescription);
            _uow.SaveChanges();

            ps.PrescriptionId = prescription.PrescriptionId;

            List<ProcedureDTO> procedures = _procedureService.CreateProceduresForPrescription(ps);
            return _prescriptionFrequencyProcedureFactory.CreatePrescriptionFrequencyProcedure(ps, procedures);
        }
    }
}

