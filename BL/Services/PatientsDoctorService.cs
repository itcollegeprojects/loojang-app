﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class PatientsDoctorService : IPatientsDoctorService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPatientsDoctorFactory _patientsDoctorFactory;
        private readonly IPatientsDoctorDetailsFactory _patientsDoctorDetailsFactory;
        private readonly IPatientToDoctorFactory _patientToDoctorFactory;
        private readonly IApplicationUserService _applicationUserService;

        public PatientsDoctorService(IAppUnitOfWork uow, IPatientsDoctorFactory patientsDoctorFactory, IPatientToDoctorFactory patientToDoctorFactory, IApplicationUserService applicationUserService, IPatientsDoctorDetailsFactory patientsDoctorDetailsFactory)
        {
            _uow = uow;
            _patientsDoctorFactory = patientsDoctorFactory;
            _patientToDoctorFactory = patientToDoctorFactory;
            _applicationUserService = applicationUserService;
            _patientsDoctorDetailsFactory = patientsDoctorDetailsFactory;
        }

        public IEnumerable<PatientsDoctorDTO> All()
        {
            return _uow.PatientDoctors.All()
                    .Select(p => _patientsDoctorFactory
                    .Create(p)).ToList(); ;
        }

        public PatientsDoctorDTO Find(int id)
        {
            PatientsDoctor patientsDoctor = _uow.PatientDoctors.Find(id);
            if (patientsDoctor != null)
            {
                return _patientsDoctorFactory.Create(patientsDoctor);
            }

            return null;
        }


        private void VerifyDTO(PatientsDoctorDTO p)
        {
            if (!_uow.Patients.Exists(p.PatientId)) throw new HttpRequestException("Patient not found");

            if (!_uow.ApplicationUsers.Exists(p.ApplicationUserId)) throw new HttpRequestException("Doctor not found");

            if (_applicationUserService.IsInRole(p.ApplicationUserId, "doctor")) throw new HttpRequestException("User is not a doctor");

            if (p.EndTime < p.StartTime) throw new HttpRequestException("End time can not be before start time");
        }

        public PatientsDoctorDTO Add(PatientsDoctorDTO p)
        {
            VerifyDTO(p);
            if (p.EndTime == null) p.EndTime = DateTime.MaxValue;

            PatientsDoctor patientsDoctor = _patientsDoctorFactory.Create(p);
            _uow.PatientDoctors.Add(patientsDoctor);
            _uow.SaveChanges();
            return _patientsDoctorFactory.Create(patientsDoctor);
        }

        public PatientToDoctorDTO Add(PatientToDoctorDTO p)
        {
            PatientsDoctor patientsDoctor = _patientToDoctorFactory.Create(p);
            _uow.PatientDoctors.Add(patientsDoctor);
            _uow.SaveChanges();
            return _patientToDoctorFactory.Create(patientsDoctor);
        }

        public PatientsDoctorDTO Update(PatientsDoctorDTO p)
        {
            VerifyDTO(p);

            PatientsDoctor patientsDoctor = _patientsDoctorFactory.Create(p);
            _uow.PatientDoctors.Update(patientsDoctor);
            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            return _patientsDoctorFactory.Create(patientsDoctor);

        }

        public PatientsDoctorDTO Delete(int id)
        {
            PatientsDoctor p = _uow.PatientDoctors.Find(id);

            if (p != null)
            {
                _uow.PatientDoctors.Remove(id);
                _uow.SaveChanges();
                return _patientsDoctorFactory.Create(p);
            }

            return null;
        }

        public IEnumerable<PatientsDoctorDetailsDTO> FindByPatientId(int patientId)
        {
            return _uow.PatientDoctors
                .FindByPatientId(patientId)
                .Select((p) => _patientsDoctorDetailsFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientsDoctorDetailsDTO> FindByApplicationUserId(string applicationUserId)
        {
            return _uow.PatientDoctors
                .FindByApplicationUserId(applicationUserId)
                .Select((p) => _patientsDoctorDetailsFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientsDoctorDetailsDTO> FindByApplicationUserIdAndPatientId(string applicationUserId, int patientId)
        {
            return _uow.PatientDoctors
                .FindByApplicationUserId(applicationUserId)
                .Where(p=>p.PatientId==patientId)
                .Select((p) => _patientsDoctorDetailsFactory.Create(p))
                .ToList();
        }

        public PatientsDoctorDTO FindByApplicationUserIdAndId(string applicationUserId, int id)
        {
            PatientsDoctor patientsDoctor = _uow.PatientDoctors.Find(id);
            if (!_uow.PatientDoctors.Exists(id)) throw new HttpRequestException();
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, patientsDoctor.PatientId)) throw new HttpRequestException();
            return _patientsDoctorFactory.Create(patientsDoctor);
        }
    }
}
