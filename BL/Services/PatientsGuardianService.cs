﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class PatientsGuardianService : IPatientsGuardianService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPatientsGuardianFactory _patientsGuardianFactory;
        private readonly IPatientsGuardianDetailsFactory _patientsGuardianDetailsFactory;
        private readonly IApplicationUserService _applicationUserService;

        public PatientsGuardianService(IAppUnitOfWork uow, IPatientsGuardianFactory patientsGuardianFactory, IApplicationUserService applicationUserService, IPatientsGuardianDetailsFactory patientsGuardianDetailsFactory)
        {
            _uow = uow;
            _patientsGuardianFactory = patientsGuardianFactory;
            _applicationUserService = applicationUserService;
            _patientsGuardianDetailsFactory = patientsGuardianDetailsFactory;
        }

        public IEnumerable<PatientsGuardianDTO> All()
        {
            return _uow.PatientGuardians.All()
            .Select(p => _patientsGuardianFactory
            .Create(p))
            .ToList();
        }

        public IEnumerable<PatientsGuardianDTO> FindByGuardianId(string applicationUserId)
        {
            return _uow.PatientGuardians.FindByGuardianId(applicationUserId)
                .Select(p => _patientsGuardianFactory
                    .Create(p))
                .ToList();
        }

        private void VerifyDTO(PatientsGuardianDTO p)
        {
            if (!_uow.Patients.Exists(p.PatientId)) throw new HttpRequestException("Patient not found");

            if (!_uow.ApplicationUsers.Exists(p.ApplicationUserId)) throw new HttpRequestException("Guardian not found");

            if (_applicationUserService.IsInRole(p.ApplicationUserId, "guardian")) throw new HttpRequestException("User is not a guardian");

            if (p.EndDate < p.StartDate) throw new HttpRequestException("End date can not be before start date");
        }
        public PatientsGuardianDTO Find(int id)
        {
            PatientsGuardian patientsGuardian = _uow.PatientGuardians.Find(id);
            if (patientsGuardian == null) return null;
            return _patientsGuardianFactory.Create(patientsGuardian);
        }

        public PatientsGuardianDTO Add(PatientsGuardianDTO p)
        {
            if (p.EndDate == null) p.EndDate = DateTime.MaxValue;
            VerifyDTO(p);

            PatientsGuardian patientsGuardian = _patientsGuardianFactory.Create(p);
            _uow.PatientGuardians.Add(patientsGuardian);

            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }
            return _patientsGuardianFactory.Create(patientsGuardian);
        }

        public PatientsGuardianDTO Update(PatientsGuardianDTO p)
        {
            VerifyDTO(p);

            PatientsGuardian patientsGuardian = _patientsGuardianFactory.Create(p);
            _uow.PatientGuardians.Update(patientsGuardian);
            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }
            return _patientsGuardianFactory.Create(patientsGuardian);
        }

        public PatientsGuardianDTO Delete(int id)
        {
            PatientsGuardian patientsGuardian = _uow.PatientGuardians.Find(id);

            if (patientsGuardian != null)
            {
                _uow.PatientGuardians.Remove(id);
                _uow.SaveChanges();
                return _patientsGuardianFactory.Create(patientsGuardian);
            }

            return null;
        }

        public IEnumerable<PatientsGuardianDetailsDTO> FindByPatientId(int patientId)
        {
            return _uow.PatientGuardians
                .FindByPatientId(patientId)
                .Select((p) => _patientsGuardianDetailsFactory.Create(p))
                .ToList();            
        }

        public IEnumerable<PatientsGuardianDetailsDTO> FindByApplicationUserIdAndPatientId(string applicationUserId, int patientId)
        {
            return _uow.PatientGuardians
                .FindByApplicationUserId(applicationUserId)
                .Where(p => p.PatientId == patientId)
                .Select((p) => _patientsGuardianDetailsFactory.Create(p))
                .ToList();
        }
    }
}
