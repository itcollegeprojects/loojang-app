﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;


namespace BL.Services
{
    public class PatientService : IPatientService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPatientFactory _patientFactory;
        private readonly IPatientBriefFactory _patientBriefFactory;
        private readonly IPatientsDoctorDetailsFactory _patientsDoctorDetailsFactory;
        private readonly IPatientsGuardianDetailsFactory _patientsGuardianDetailsFactory;
        private readonly IPatientsRoomFactory _patientsRoomFactory;

        public PatientService(IAppUnitOfWork uow, IPatientFactory patientFactory, IPatientsRoomFactory patientsRoomFactory, IPatientBriefFactory patientBriefFactory, IPatientsDoctorDetailsFactory patientsDoctorDetailsFactory, IPatientsGuardianDetailsFactory patientsGuardianDetailsFactory)
        {
            _uow = uow;
            _patientFactory = patientFactory;
            _patientsRoomFactory = patientsRoomFactory;
            _patientBriefFactory = patientBriefFactory;
            _patientsDoctorDetailsFactory = patientsDoctorDetailsFactory;
            _patientsGuardianDetailsFactory = patientsGuardianDetailsFactory;
        }

        public IEnumerable<PatientDTO> All()
        {
            return _uow.Patients.All()
                    .Select(p =>
                        {
                            p.PatientRooms =
                                new List<PatientRoom> (new [] {_uow.PatientRooms.FindPresentRoomByPatientId(p.PatientId)});
                            return p;
                        })
                    .Select(p =>
                        {
                            PatientDTO dto = _patientFactory.Create(p);
                            PatientRoom r = p.PatientRooms.FirstOrDefault();
                            if (r != null)
                            {
                                dto.Room = r.Room.RoomCode ?? "-";
                            }
                            else
                            {
                                dto.Room = "-";
                            }
                            
                            
                            return dto;
                        })
                    .ToList();
        }

        public IEnumerable<PatientBriefDTO> AllPresentPatients()
        {
            return _uow.Patients.AllPresentPatients()
                .Select(p => _patientBriefFactory.Create(p))
                .ToList();                
        }

        public PatientDTO Find(int id)
        {
            Patient p = _uow.Patients.Find(id);

            if (p != null)
            {
                return _patientFactory.Create(_uow.Patients.Find(id));
            }

            return null;
        }

        public PatientDTO Add(PatientDTO p)
        {
            Patient patient = _patientFactory.Create(p);
            patient.LeftDate = DateTime.MaxValue;
            _uow.Patients.Add(patient);
            _uow.SaveChanges();
            return _patientFactory.Create(patient);
        }

        public PatientDTO Update(PatientDTO p)
        {

            Patient patient = _patientFactory.Create(p);
            _uow.Patients.Update(patient);
            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            return _patientFactory.Create(patient);
        }

        public PatientDTO Delete(int id)
        {
            Patient p = _uow.Patients.Find(id);

            if (p != null)
            {
                _uow.Patients.Remove(id);
                _uow.SaveChanges();
                return _patientFactory.Create(p);
            }

            return null;
        }
        public IEnumerable<PatientsRoomDTO> GetRoomsByPatientId(int id)
        {
            return _uow.PatientRooms
                .FindByPatientId(id)
                .Select(p => _patientsRoomFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientsDoctorDetailsDTO> GetDoctorsByPatientId(int id)
        {
            return _uow.PatientDoctors
                .FindByPatientId(id)
                .Select(p => _patientsDoctorDetailsFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientsGuardianDetailsDTO> GetGuardiansByPatientId(int id)
        {
            return _uow.PatientGuardians
                .FindByPatientId(id)
                .Select(p => _patientsGuardianDetailsFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientDTO> FindByPersonalCode(string personalcode)
        {
            return _uow.Patients.All()
                .Where(p => p.PersonalCode == personalcode)
                .Select(p=> _patientFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientDTO> AllByGuardianId(string applicationUserId)
        {
            return _uow.Patients.AllByGuardianId(applicationUserId)
                .Select(p => _patientFactory.Create(p))
                .ToList();
        }

        public IEnumerable<PatientDTO> AllByDoctorsId(string applicationUserId)
        {
            return _uow.Patients.AllByDoctorsId(applicationUserId)
                .Select(p => _patientFactory.Create(p))
                .ToList();
        }
        public IEnumerable<PatientDTO> All(string applicationUserId)
        {
            
            var patients = _uow.Patients.AllByApplicationUserId(applicationUserId).ToList();

            var result = patients
                    .Select(p =>
                    {
                        p.PatientRooms =
                            new List<PatientRoom>(new[] { _uow.PatientRooms.FindPresentRoomByPatientId(p.PatientId) });
                        return p;
                    })
                    .Select(p =>
                    {
                        PatientDTO dto = _patientFactory.Create(p);
                        PatientRoom r = p.PatientRooms.FirstOrDefault();
                        if (r != null)
                        {
                            dto.Room = r.Room.RoomCode ?? "-";
                        }
                        else
                        {
                            dto.Room = "-";
                        }


                        return dto;
                    })
                    .ToList();

            return result;

        }

        IEnumerable<PatientDTO> IPatientService.AllPresentPatients()
        {
            return _uow.Patients.AllPresentPatients()
                .Select(p => _patientFactory.Create(p))
                .ToList();
        }
    }
}
