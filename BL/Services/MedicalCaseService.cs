﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class MedicalCaseService : IMedicalCaseService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IMedicalCaseFactory _medicalCaseFactory;

        public MedicalCaseService(IAppUnitOfWork uow, IMedicalCaseFactory medicalCaseFactory)
        {
            _uow = uow;
            _medicalCaseFactory = medicalCaseFactory;
        }

        public IEnumerable<MedicalCaseDTO> All()
        {
            return _uow.MedicalCases.AllRelated()
                .Select(p => _medicalCaseFactory.Create(p))
                .ToList(); ;
        }

        public MedicalCaseDTO Find(int id)
        {
            MedicalCase medicalCase = _uow.MedicalCases.Find(id);
            if (medicalCase == null) return null;
            return _medicalCaseFactory.Create(medicalCase);
        }

        public MedicalCaseDTO FindForGuardian(int id, string applicationUserId)
        {
            MedicalCase medicalCase = _uow.MedicalCases.Find(id);
            if (!_uow.PatientGuardians.IsGuardian(applicationUserId, medicalCase.PatientsDoctor.PatientId)) throw new HttpRequestException();
            return _medicalCaseFactory.Create(medicalCase);

        }

        public MedicalCaseDTO Add(MedicalCaseDTO p)
        {
            if (!_uow.PatientDoctors.Exists(p.PatientsDoctorId)) throw new HttpRequestException("PatientsDoctor does not exist");

            p.CreatedOn = DateTime.Now;
            MedicalCase medicalCase = _medicalCaseFactory.Create(p);
            _uow.MedicalCases.Add(medicalCase);
            try
            {
                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            var mc = _uow.MedicalCases.Find(medicalCase.MedicalCaseId);
            return _medicalCaseFactory.Create(mc);
        }

        public MedicalCaseDTO Update(MedicalCaseDTO p)
        {
            if (!_uow.PatientDoctors.Exists(p.PatientsDoctorId)) throw new HttpRequestException("PatientsDoctor does not exist");

            MedicalCase medicalCase = _medicalCaseFactory.Create(p);
            _uow.MedicalCases.Update(medicalCase);
            _uow.SaveChanges();
            return _medicalCaseFactory.Create(medicalCase);
        }

        public MedicalCaseDTO Delete(int id)
        {
            MedicalCase p = _uow.MedicalCases.Find(id);

            if (p != null)
            {
                _uow.MedicalCases.Remove(id);
                _uow.SaveChanges();
                return _medicalCaseFactory.Create(p);
            }

            return null;
        }

        public IEnumerable<MedicalCaseDTO> FindByPatientsDoctorsId(int patientsDoctorId)
        {
            return _uow.MedicalCases
                .FindByPatientsDoctorId(patientsDoctorId)
                .Select((p) => _medicalCaseFactory.Create(p))
                .ToList();
        }

        public IEnumerable<MedicalCaseDTO> FindByDoctorId(string id)
        {
            return _uow.MedicalCases
                .FindByDoctorId(id)
                .Select((p) => _medicalCaseFactory.Create(p))
                .ToList();
        }

        public IEnumerable<MedicalCaseDTO> FindByPatientId(int id)
        {
            return _uow.MedicalCases
                .FindByPatientId(id)
                .Select((p) => _medicalCaseFactory.Create(p))
                .ToList();
        }

        public IEnumerable<MedicalCaseDTO> AllByGuardianId(string applicationUserId)
        {
            var patientIds = _uow.PatientGuardians.FindByGuardianId(
                applicationUserId).Select(p => p.PatientId).ToList();

            return _uow.MedicalCases
                .FindByPatientIds(patientIds)
                .Select((p) => _medicalCaseFactory.Create(p))
                .ToList();
        }
    }
}
