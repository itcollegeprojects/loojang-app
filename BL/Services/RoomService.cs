﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class RoomService : IRoomService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IRoomFactory _roomFactory;
        private readonly IRoomCodeFactory _roomCodeFactory;
        private readonly IRoomsPatientFactory _roomsPatientFactory;

        public RoomService(IAppUnitOfWork uow, IRoomFactory roomFactory, IRoomCodeFactory roomCodeFactory,
            IRoomsPatientFactory roomsPatientFactory)
        {
            _uow = uow;
            _roomFactory = roomFactory;
            _roomCodeFactory = roomCodeFactory;
            _roomsPatientFactory = roomsPatientFactory;
        }

        public IEnumerable<RoomDTO> All()
        {
            return _uow.Rooms.All()
                    .Select(r => _roomFactory.Create(r))
                    .ToList();
        }

        public RoomDTO Find(int id)
        {
            Room r = _uow.Rooms.Find(id);

            if (r != null)
            {
                return _roomFactory.Create(_uow.Rooms.Find(id));
            }

            return null;
        }

        public RoomDTO Add(RoomDTO dto)
        {
            Room room = _roomFactory.Create(dto);
            _uow.Rooms.Add(room);
            _uow.SaveChanges();
            return _roomFactory.Create(room);
        }

        public RoomDTO Update(RoomDTO dto)
        {
            Room room = _roomFactory.Create(dto);
            _uow.Rooms.Update(room);
            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            return _roomFactory.Create(room);
        }

        public RoomDTO Delete(int id)
        {
            Room room = _uow.Rooms.Find(id);

            if (room != null)
            {
                _uow.Rooms.Remove(id);
                _uow.SaveChanges();
                return _roomFactory.Create(room);
            }

            return null;
        }

        public IEnumerable<RoomsPatientDTO> GetPatientsByRoomId(int roomId)
        {
            return _uow.PatientRooms
                        .FindByRoomId(roomId)
                        .Select(p => _roomsPatientFactory.Create(p))
                        .ToList();
        }

        public IEnumerable<RoomCodeDTO> GetFreeRooms()
        {
            return _uow.Rooms.GetFreeRooms()
                .Select(r => _roomCodeFactory.Create(r))
                .ToList();
        }

        public IEnumerable<RoomDTO> FindByRoomCode(string code)
        {
            return _uow.Rooms.All()
                .Where(p => p.RoomCode == code)
                .Select(p => _roomFactory.Create(p))
                .ToList();
        }
    }
}
