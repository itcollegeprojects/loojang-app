﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class ApplicationUserService : IApplicationUserService
    {

        private readonly IAppUnitOfWork _uow;
        private readonly IApplicationUserFactory _applicationUserFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ApplicationUserService(IAppUnitOfWork uow, IApplicationUserFactory applicationUserFactory, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _uow = uow;
            _applicationUserFactory = applicationUserFactory;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public IEnumerable<ApplicationUserDTO> All()
        {
            return _uow.ApplicationUsers.AllAsync()
                .Result
                .Select(p => _applicationUserFactory.Create(p))
                .ToList();
        }

        public IEnumerable<ApplicationUserDTO> AllByRole(string role)
        {
            return _userManager.GetUsersInRoleAsync(role.ToUpper()).Result
                .Select(p => _applicationUserFactory.Create(p))
                .ToList();
        }

        public ApplicationUserDTO Find(string id)
        {
            ApplicationUser applicationUser = _uow.ApplicationUsers.FindAsync(id).Result;
            if (applicationUser != null)
            {
                return _applicationUserFactory.Create(applicationUser);
            }

            return null;
        }

        public ApplicationUserDTO Add(ApplicationUserDTO p)
        {
            ApplicationUser applicationUser = _applicationUserFactory.Create(p);
            _uow.ApplicationUsers.Add(applicationUser);
            _uow.SaveChanges();
            return _applicationUserFactory.Create(applicationUser);
        }

        public ApplicationUserDTO AddToRole(string userId, string newRole)
        {
            //do not use EFApplicationUserRepository to get user because it returns an object that is being tracked and thus unable to update
            //ApplicationUser applicationUser = _uow.ApplicationUsers.FindWithoutTracking(userId);
            var applicationUser = _userManager.Users.FirstOrDefault(p => p.Id == userId);
            if (applicationUser == null) return null;

            newRole = newRole.Substring(0, 1).ToUpper() + newRole.Substring(1).ToLower();

            var oldRoles = _userManager.GetRolesAsync(applicationUser);               

            foreach (string oldRole in oldRoles.Result)
            {
                if (oldRole == newRole) continue;
                var removeRoleResult = _userManager.RemoveFromRoleAsync(applicationUser, oldRole).Result;
            }

            try
            {
                var result = _userManager.AddToRoleAsync(applicationUser, newRole).Result;
                if (!result.Succeeded && result.Errors.First().Code != "UserAlreadyInRole")
                {
                    return null;
                }
            }
            catch (System.AggregateException e)
            {

            }

            return _applicationUserFactory.Create(applicationUser);
        }

        public async Task<ApplicationUserDTO> RemoveFromRole(string userId, string oldRole)
        {
            ApplicationUser applicationUser =_uow.ApplicationUsers.FindAsync(userId).Result;
            if (applicationUser == null)
            {
                return null;
            }

            oldRole = oldRole.Substring(0, 1).ToUpper() + oldRole.Substring(1).ToLower();

            var result = await _userManager.RemoveFromRoleAsync(applicationUser, oldRole);
            if (!result.Succeeded)
            {
                return null;
            }

            return _applicationUserFactory.Create(applicationUser);
        }

        public bool IsInRole(string applicationUserId, string role)
        {
            ApplicationUser user = _uow.ApplicationUsers.Find(applicationUserId);
            Task<IList<string>> roles = _userManager.GetRolesAsync(user);
            if (!roles.Result.ToList().ConvertAll(d => d.ToLower()).Contains(role)) return true;

            return false;

        }

        public ApplicationUserDTO Update(ApplicationUserDTO p)
        {
            ApplicationUser applicationUser = _uow.ApplicationUsers.Find(p.ApplicationUserId);

            if (p.FirstName != null) applicationUser.FirstName = p.FirstName;
            if (p.LastName != null) applicationUser.LastName = p.FirstName;
            if (p.Email != null) applicationUser.Email = p.Email;
            //            var result = await _userManager.AddToRolesAsync(applicationUser, p.Roles);
            //            if (!result.Succeeded)
            //            {
            //                return null;
            //            }

            _uow.ApplicationUsers.Update(applicationUser);

            try
            {
                _uow.SaveChanges();

            }
            catch (DbUpdateConcurrencyException e)
            {
                return null;
            }

            return _applicationUserFactory.Create(applicationUser);
        }

        public ApplicationUserDTO Delete(string id)
        {
            ApplicationUser p = _uow.ApplicationUsers.Find(id);

            if (p != null)
            {
                _uow.ApplicationUsers.Remove(id);
                _uow.SaveChanges();
                return _applicationUserFactory.Create(p);
            }

            return null;
        }
    }
}
