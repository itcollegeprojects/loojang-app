﻿using DAL.App.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace BL.Services
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                // Look for any FrequencyTypes
                if (context.FrequencyTypes.Any())
                {
                    // DB has already been seeded
                    return;
                }

                context.FrequencyTypes.AddRange(
                     new FrequencyType
                     {
                         FrequencyTypeName = "1 time per day",
                         Comment = "1 time per day after breakfast at 8:00",
                         Weekday ="0,1,2,3,4,5,6",
                         Hour = "8"
                     },
                    new FrequencyType
                    {
                        FrequencyTypeName = "3 times per day",
                        Comment = "3 times per day after eating at 8:00, 13:00, 20:00",
                        Weekday = "0,1,2,3,4,5,6",
                        Hour = "8,13,20"
                    },
                    new FrequencyType
                    {
                        FrequencyTypeName = "1 times per day in work days",
                        Comment = "1 times per day in work days after breakfast at 8:00",
                        Weekday = "0,1,2,3,4",
                        Hour = "8"
                    },
                    new FrequencyType
                    {
                        FrequencyTypeName = "5 times per day on Monday",
                        Comment = "5 times per day on Monday at 8:00, 12:00, 14:00, 16:00, 18:00",
                        Weekday = "0",
                        Hour = "8,12,14,16,18"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
