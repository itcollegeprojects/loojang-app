﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Patient
    {
        public int PatientId { get; set; }
        [Required]
        [MaxLength(20), MinLength(11)]
        public string PersonalCode { get; set; }
        [Required]
        [MaxLength(128)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(128)]
        public string LastName { get; set; }

        public DateTime BirthDay { get; set; }

        public DateTime JoinedDate { get; set; }

        public DateTime? LeftDate { get; set; }

        [MaxLength(2000)]
        public string Comment { get; set; }

        public List<PatientsDoctor> PatientsDoctors { get; set; }

        public List<PatientsGuardian> PatientsGuardians { get; set; }

        public List<PatientRoom> PatientRooms { get; set; }

        public string FirstLastName => $"{FirstName} {LastName}";
        public string LastFirstName => $"{LastName} {FirstName}";

    }
}