﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class  PatientsGuardian
    {
        public int PatientsGuardianId{ get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey(nameof(ApplicationUserId))]
        public ApplicationUser ApplicationUser { get; set; }
        
        public int PatientId { get; set; }
        
        public Patient Patient { get; set; }
    }

}