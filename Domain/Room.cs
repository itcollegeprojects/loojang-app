﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;

namespace Domain
{
    public class Room
    {
        public int RoomId { get; set; }
        [Required]
        [MaxLength(10)]
        public string RoomCode { get; set; }
        [MaxLength(2000)]
        public string Comment { get; set; }
        [Required]
        public int Capacity { get; set; }

        public List<PatientRoom> PatientRooms{ get; set; }

        public int Free => Capacity - PatientRooms.Count(p => 
                               (p.FromDate < DateTime.Today && p.ToDate > DateTime.Today) ||
                               (p.FromDate < DateTime.Today && p.ToDate == null));                           
    }
}