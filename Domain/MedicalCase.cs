﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class MedicalCase
    {
        public int MedicalCaseId { get; set; }

        public DateTime CreatedOn { get; set; }

        [MaxLength(2000)]
        public string Comment { get; set; }

        public int PatientsDoctorId { get; set; }
        [ForeignKey(nameof(PatientsDoctorId))]
        public PatientsDoctor PatientsDoctor { get; set; }

        public List<Prescription> Prescriptions { get; set; }

    }
}
