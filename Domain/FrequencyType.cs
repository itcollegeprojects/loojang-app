﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class FrequencyType
    {
        public int FrequencyTypeId { get; set; }
        [Required]
        [MaxLength(100)]
        public string FrequencyTypeName { get; set; }
        [MaxLength(300)]
        public string Comment { get; set; }
        [MaxLength(300)]
        public string Weekday { get; set; }
        [MaxLength(300)]
        public string Hour { get; set; }

    }
}