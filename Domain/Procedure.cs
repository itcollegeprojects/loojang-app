﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Procedure
    {
        public int ProcedureId { get; set; }

        public DateTime ScheduledDate { get; set; }

        public DateTime? ActualDate { get; set; }

        public int PrescriptionId { get; set; }

        public Prescription Prescription { get; set; }

        [MaxLength(300)]
        public string Comment { get; set; }

        public string CaretakerId { get; set; }
        [ForeignKey(nameof(CaretakerId))]
        public ApplicationUser Caretaker { get; set; }

    }
}