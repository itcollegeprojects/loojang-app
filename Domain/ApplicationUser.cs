﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class ApplicationUser : IdentityUser

    {

        [MaxLength(128)]
        public string FirstName { get; set; }
        [MaxLength(128)]
        public string LastName { get; set; }

        [InverseProperty(nameof(PatientsDoctor.ApplicationUser))]
        public List<PatientsDoctor> PatientsDoctors { get; set; }

        [InverseProperty(nameof(PatientsGuardian.ApplicationUser))]
        public List<PatientsGuardian> PatientsGuardians { get; set; }

        [InverseProperty(nameof(Procedure.Caretaker))]
        public List<Procedure> Procedures { get; set; }

        public string FirstLastName => $"{FirstName} {LastName}";
        public string LastFirstName => $"{LastName} {FirstName}";

    }
}