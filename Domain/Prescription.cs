﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Prescription
    {
        public int PrescriptionId { get; set; }

        [MaxLength(300)]
        public string Description { get; set; }

        [MaxLength(1000)]
        public string Comment{ get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int FrequencyTypeId { get; set; }

        [ForeignKey(nameof(FrequencyTypeId))]
        public FrequencyType FrequencyType { get; set; }
  
        public int MedicalCaseId { get; set; }

        [ForeignKey(nameof(MedicalCaseId))]
        public MedicalCase MedicalCase { get; set; }

        public List<Procedure> Procedures { get; set; }
    }
}