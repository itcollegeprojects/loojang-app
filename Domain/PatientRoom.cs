﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class PatientRoom
    {
        public int PatientRoomId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        [ForeignKey("Room")]
        public int RoomId { get; set; }
        
        public Room Room { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        
        public Patient Patient{ get; set; }
    }
}