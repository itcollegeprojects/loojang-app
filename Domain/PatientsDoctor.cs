﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class PatientsDoctor
    {
        public int PatientsDoctorId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public int PatientId { get; set; }
        
        public Patient Patient { get; set; }
        
        public String ApplicationUserId { get; set; }
        [ForeignKey(nameof(ApplicationUserId))]
        public ApplicationUser ApplicationUser { get; set; }

        [InverseProperty(nameof(MedicalCase.PatientsDoctor))]
        public List<MedicalCase> MedicalCases { get; set; }
    }
}